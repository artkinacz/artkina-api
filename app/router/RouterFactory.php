<?php

namespace App;

use Nette;
use Nette\Application\Routers\Route;
use Nette\Application\Routers\RouteList;


/**
 * Router factory.
 */
class RouterFactory
{

	/**
	 * @return \Nette\Application\IRouter
	 */
	public function createRouter()
	{

		$router = new RouteList();

		$routerApi = new RouteList("Api");
		$routerApi[] = new Route('api/notification/add/<type>/<userId>/<projectionId>/<minutes>', 'Notification:add');
		$routerApi[] = new Route('api/notification/edit/<notifId>/<minutes>', 'Notification:edit');
		$routerApi[] = new Route('api/notification/<action>/<notifId>', 'Notification:delete');
		$routerApi[] = new Route('api/image/<width>/<height>/<filename>/<crop=0>', "Image:default");
		$routerApi[] = new Route('api', "Homepage:default");

		$routerApi[] = new Route('api/projection/list/<slugs>/<until>[/<since>]', array(
			"presenter" => "Projection",
			"action" => "list",
			"until" => NULL,
			"since" => NULL
		));
		$routerApi[] = new Route('api/<presenter=Homepage>/<action=default>[/<id>]');

		$router[] = $routerApi;
		$router[] = new Route("", "Front:Homepage:default");

		return $router;
	}

}
