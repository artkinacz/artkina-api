<?php

namespace App\Doctrine\Facade;

use App\Doctrine\Entity\Tmdb;
use App\Doctrine\Entity\Movie;
use BG\Doctrine\Facade\BaseFacade;

/**
 * Class TmdbFacade
 * @package App\Doctrine\Facade
 * @author Jan Blasko
 */
class TmdbFacade extends BaseFacade
{

	/**
	 * @param $tmdbData
	 * @param Tmdb $tmdbEntity
	 * @return Tmdb
	 * @return Imdb
	 */
	public function fillEntityByMovieData(Tmdb $tmdbEntity, $tmdbData, $imdbData)
	{

		$tmdbData->genre_ids = implode(';', $tmdbData->genre_ids);

		$tmdbEntity
			->setTmdbId($tmdbData->id)
			->setAdult($tmdbData->adult)
			->setBackdropPath($tmdbData->backdrop_path)
			->setPosterPath($tmdbData->poster_path)
			->setOriginalLanguage($tmdbData->original_language)
			->setTitle($tmdbData->title)
			->setOriginalTitle($tmdbData->original_title)
			->setOverview($tmdbData->overview)
			->setGenreIds($tmdbData->genre_ids)
			->setReleaseDate(new \DateTime($tmdbData->release_date))
			->setPopularity($tmdbData->popularity)
			->setVoteAverage($tmdbData->vote_average)
			->setVoteCount($tmdbData->vote_count)
			->setImdbId($tmdbData->imdb_id)
			->setImdbRating($imdbData->imdbRating);

		return $tmdbEntity;
	}

}