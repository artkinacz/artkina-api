<?php

namespace BG\Doctrine\Facade;

use Kdyby\Doctrine\EntityDao;
use Kdyby\Doctrine\InvalidArgumentException;
use Kdyby\Doctrine\QueryException;
use Kdyby\Persistence\ObjectDao;
use Nette\Object;

/**
 * Class BaseFacade
 * @package BG\Doctrine\Facade
 * @author Jaromír Navara
 */
class BaseFacade extends Object
{

	/** @var EntityDao */
	protected $dao;

	public function __construct(EntityDao $dao)
	{
		$this->setDao($dao);
	}

	/**
	 * @param \Kdyby\Doctrine\EntityDao $dao
	 */
	public function setDao($dao)
	{
		$this->dao = $dao;

		return $this;
	}

	/**
	 * @return \Kdyby\Doctrine\EntityDao
	 */
	public function getDao()
	{
		return $this->dao;
	}

	/**
	 * Persists given entities and flushes all to the storage.
	 *
	 * @param object|array|\Doctrine\Common\Collections\Collection $entity
	 * @return object|array
	 */
	public function save($entities)
	{
		$result = FALSE;
		if (is_array($entities) || $entities instanceof \Traversable) {
			foreach ($entities as $entity) {
				$this->dao->add($entity);
			}
		} else {
			$this->dao->add($entities);
		}

		return $this->dao->save();
	}

	/**
	 * @param object|array|\Traversable $entity
	 * @param bool $flush
	 * @throws InvalidArgumentException
	 */
	public function delete($entities, $flush = ObjectDao::FLUSH)
	{
		return $this->getDao()->delete($entities, $flush);
	}

	/**
	 * Finds all entities in the repository.
	 *
	 * @return array The entities.
	 */
	public function findAll()
	{
		return $this->getDao()->findAll();
	}

	/**
	 * Fetches all records like $key => $value pairs
	 *
	 * @param array $criteria
	 * @param string $value
	 * @param string $key
	 *
	 * @throws \Exception|QueryException
	 * @return array
	 */
	public function findPairs($criteria, $value = NULL, $key = 'id')
	{
		return $this->getDao()->findPairs($criteria, $value, $key);
	}

	/**
	 * Fetches all records and returns an associative array indexed by key
	 *
	 * @param array $criteria
	 * @param string $key
	 *
	 * @throws \Exception|QueryException
	 * @return array
	 */
	public function findAssoc($criteria, $key = NULL)
	{
		return $this->getDao()->findAssoc($criteria, $key);
	}

	/**
	 * @param string $dql DQL Query
	 * @return \Doctrine\ORM\Query
	 */
	public function createQuery($dql)
	{
		return $this->getDao()->createQuery($dql);
	}

	public function createQueryBuilder($alias = NULL, $indexBy = NULL)
	{
		return $this->getDao()->createQueryBuilder($alias, $indexBy);
	}

	/**
	 * Finds entities by a set of criteria.
	 *
	 * @param array|null $criteria
	 * @param array|null $orderBy
	 * @param int|null $limit
	 * @param int|null $offset
	 *
	 * @return array The objects.
	 */
	public function findBy(array $criteria = NULL, array $orderBy = NULL, $limit = NULL, $offset = NULL)
	{
		if (!is_array($criteria)) {
			$criteria = array();
		}

		return $this->getDao()->findBy($criteria, $orderBy, $limit, $offset);
	}

	/**
	 * Finds a single entity by a set of criteria.
	 *
	 * @param array $criteria
	 * @param array|null $orderBy
	 *
	 * @return object|null The entity instance or NULL if the entity can not be found.
	 */
	public function findOneBy(array $criteria, array $orderBy = NULL)
	{
		return $this->getDao()->findOneBy($criteria, $orderBy);
	}

	/**
	 * Returns Doctrine's Event manager
	 * @return \Doctrine\Common\EventManager
	 */
	public function getEventManager()
	{
		return $this->getDao()->getEntityManager()->getEventManager();
	}

}