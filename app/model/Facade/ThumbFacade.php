<?php

namespace App\Doctrine\Facade;

use BG\Doctrine\Facade\BaseFacade;

/**
 * Class ThumbFacade
 * @package App\Doctrine\Facade
 * @author Jaromír Navara
 */
class ThumbFacade extends BaseFacade
{

}