<?php

namespace App\Doctrine\Facade;

use App\Doctrine\Entity\Projection;
use BG\Doctrine\Facade\BaseFacade;

/**
 * Class ProjectionFacade
 * @package App\Doctrine\Facade
 * @author Jaromír Navara
 */
class ProjectionFacade extends BaseFacade
{

	public function disableDaysProjections($day, $cinema)
	{
		$this->dao->createQueryBuilder()
			->update(Projection::getClassName(), 'p')
			->set('p.enabled', 0)
			->andWhere('DATE(p.date) = :date')
			->andWhere('p.cinema = :cinema')
			->setParameters([
				'date' => $day,
				'cinema' => $cinema,
			])->getQuery()->execute();
	}

	public function getProjections($since, $until, $cinemas)
	{
		return $this->dao->createQueryBuilder('p')
			->select('p,m')
			->leftJoin('p.movie', 'm')
			->andWhere('p.date BETWEEN :since AND :until')
			->andWhere('p.cinema IN (:cinemas)')
			->andWhere('p.enabled = 1')
			->orderBy('p.date', 'desc')
			->setParameters([
				"since" => $since,
				"until" => $until,
				"cinemas" => $cinemas
			])->getQuery()->getResult();
	}
}