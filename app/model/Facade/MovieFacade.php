<?php

namespace App\Doctrine\Facade;

use App\Logger\Logger;
use BG\Doctrine\Facade\BaseFacade;
use Tracy\Debugger;


/**
 * Class MovieFacade
 * @package App\Doctrine\Facade
 * @author Jaromír Navara
 */
class MovieFacade extends BaseFacade
{

	public $thumbnailPath;

	/**
	 * @var Logger
	 */
	public $logger;

	/***/
	public function saveImage($url, $overwrite = FALSE)
	{
		$urlParts = explode("/", $url);
		$filename = array_pop($urlParts);
		$targetFilePath = $this->getThumbnailPath() . DIRECTORY_SEPARATOR . $filename;
		if (file_exists($targetFilePath) && !$overwrite) {
			return $filename;
		}
		if (!is_dir(dirname($targetFilePath))) {
			@mkdir(dirname($targetFilePath), 0777, TRUE);
		}
		if ($data = $this->downloadImage($url)) {
			@file_put_contents($targetFilePath, $data);
		} else {
			return NULL;
		}

		return $filename;
	}

	/**
	 * @param mixed $thumbnailPath
	 * @return $this
	 */
	public function setThumbnailPath($thumbnailPath)
	{
		$this->thumbnailPath = $thumbnailPath;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getThumbnailPath()
	{
		return $this->thumbnailPath;
	}

	/**
	 * @param Logger $logger
	 */
	public function setLogger(Logger $logger)
	{
		$this->logger = $logger;
	}

	public function downloadImage($url)
	{
		$conn = curl_init($url);
		curl_setopt($conn, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($conn, CURLOPT_FRESH_CONNECT, TRUE);
		curl_setopt($conn, CURLOPT_RETURNTRANSFER, 1);
		$url_get_contents_data = (curl_exec($conn));
		if (curl_errno($conn) !== 0) {
			Debugger::log("Image download error - " . curl_error($conn));

			return NULL;
		}
		$info = curl_getinfo($conn);
		if ($info['http_code'] != 200) {
			Debugger::log("Image download error - returned HTTP " . $info['http_code']);

			return NULL;
		}
		curl_close($conn);

		return $url_get_contents_data;
	}

	public function getMovies($moviesId)
	{
		return $this->dao->createQueryBuilder('m', 'm.id')
			->select('m,c')
			->leftJoin('m.csfd', 'c')
			->andWhere('m.id IN (:ids)')
			->setParameter('ids', $moviesId)
			->getQuery()->getResult();
	}


	public function getMoviesToTmdbUpdate($limit)
	{
		return $this->dao->createQueryBuilder('m', 'm.id')
			->select('m,t')
			->leftJoin('m.tmdb', 't')
			->where('(m.tmdb_status IS NULL OR m.tmdb_status = 1) AND (m.tmdb_update < :date OR m.tmdb_update IS NULL)')
			->orderBy('m.id', 'DESC')
			->setMaxResults($limit)
			->setParameter('date', (new \DateTime())->modify('-7 days'))
			->getQuery()->getResult();
	}

}