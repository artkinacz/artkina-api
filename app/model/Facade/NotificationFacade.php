<?php

namespace App\Doctrine\Facade;

use App\Doctrine\Entity\Notification;
use BG\Doctrine\Facade\BaseFacade;
use BG\Push\Android;
use BG\Push\Ios;

/**
 * Class NotificationFacade
 * @package App\Doctrine\Facade
 * @author Jaromír Navara
 */
class NotificationFacade extends BaseFacade
{

	/** @var Android */
	public $androidPush;

	/** @var Ios */
	public $iosPush;

	public function send(Notification $notification)
	{
		switch ($notification->getType()) {
			case Notification::DEVICE_ANDROID:
				echo($this->androidPush->send($notification));
				break;
			case Notification::DEVICE_IOS:
				$this->iosPush->send($notification);
				break;
		}
		$notification->setSent(new \DateTime());
		$this->save($notification);
	}

	/**
	 * @param \BG\Push\Android $androidPush
	 */
	public function setAndroidPush($androidPush)
	{
		$this->androidPush = $androidPush;
	}

	/**
	 * @return \BG\Push\Android
	 */
	public function getAndroidPush()
	{
		return $this->androidPush;
	}

	/**
	 * @param Ios $iosPush
	 */
	public function setIosPush($iosPush)
	{
		$this->iosPush = $iosPush;
	}

	/**
	 * @return Ios
	 */
	public function getIosPush()
	{
		return $this->iosPush;
	}


}