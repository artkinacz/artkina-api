<?php

namespace App\Doctrine\Facade;

use App\Doctrine\Entity\Csfd;
use App\Doctrine\Entity\Movie;
use BG\Doctrine\Facade\BaseFacade;

/**
 * Class CsfdFacade
 * @package App\Doctrine\Facade
 * @author Jaromír Navara
 */
class CsfdFacade extends BaseFacade
{

	/**
	 * @param $csfdMovieDetail
	 * @param Csfd $csfdEntity
	 * @param Movie $movieEntity
	 * @return Csfd
	 */
	public function fillEntityByCsfdData($csfdMovieDetail, Csfd $csfdEntity)
	{

		$csfdMovieDetail->names->cs = strip_tags($csfdMovieDetail->names->cs);

		$csfdEntity->setTitleCs($csfdMovieDetail->names->cs)
			->setPosterUrl($csfdMovieDetail->poster_url)
			->setAccessibility(isset($csfdMovieDetail->content_rating) ? $csfdMovieDetail->content_rating : NULL)
			->setRating(isset($csfdMovieDetail->rating) ? $csfdMovieDetail->rating : NULL);


		if (is_array($csfdMovieDetail->countries)) {
			$csfdEntity->setCountries(implode(", ", $csfdMovieDetail->countries));
		}

		$csfdEntity->setCsfdId($csfdMovieDetail->id);

		if (is_array($csfdMovieDetail->genres)) {
			$csfdEntity->setGenres(implode(", ", $csfdMovieDetail->genres));
		}

		$csfdEntity->setLength(isset($csfdMovieDetail->runtime) ? $csfdMovieDetail->runtime : NULL)
			->setDescription($csfdMovieDetail->plot)
			->setYear($csfdMovieDetail->year);
		if (isset($csfdMovieDetail->names->en)) {
			$csfdEntity->setTitleEn($csfdMovieDetail->names->en);
		}
		if (isset($csfdMovieDetail->names->sk)) {
			$csfdEntity->setTitleSk($csfdMovieDetail->names->sk);
		}

		if (isset($csfdMovieDetail->authors->directors)) {
			$directors = array();
			foreach ($csfdMovieDetail->authors->directors as $director) {
				$directors[] = $director->name;
			}
			if (!empty($directors)) {
				$directors = implode(", ", $directors);
			} else {
				$directors = NULL;
			}
			$csfdEntity->setDirector($directors);
		}

		return $csfdEntity;
	}

}