<?php

namespace BG\Push;

use App\Doctrine\Entity\Notification;

interface IPushSender
{
	public function send(Notification $entity);
}