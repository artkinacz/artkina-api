<?php

namespace BG\Push;

use App\Doctrine\Entity\Notification;
use GCMPushMessage;
use Nette\Application\BadRequestException;

class Android implements IPushSender
{

	/** @var \GCMPushMessage $sender */
	protected $sender;

	public function __construct(GCMPushMessage $sender)
	{
		$this->setSender($sender);
	}

	public function send(Notification $notification)
	{
		if ($notification->getType() != Notification::DEVICE_ANDROID) {
			throw new BadRequestException("Notification is not for Android device!");
		}

		/*
		 * At se notifikace pusti vzdy v cas, ktery si uzivatel nastavi.
		 * A to takovyto text: “Za XX začne promítání filmu YY v kině ZZ”
		 * kde XX ať je buď v minutách nebo v hodinách a minutách a nebo v dních, hodinách a minutách.
		 */

		$projection = $notification->getProjection();
		$startsIn = $notification->getTimestamp()->diff($projection->getDate());
		$timeText = array();
		if ($startsIn->d) {

			switch ($startsIn->d) {
				case 1:
					$suffix = " den";
					break;
				case 2:
				case 3:
				case 4:
					$suffix = " dny";
					break;
				default:
					$suffix = " dnů";
					break;
			}

			$timeText[] = $startsIn->d . $suffix;

		}
		if ($startsIn->h) {

			switch ($startsIn->h) {
				case 1:
					$suffix = " hodinu";
					break;
				case 2:
				case 3:
				case 4:
					$suffix = " hodiny";
					break;
				default:
					$suffix = " hodin";
					break;
			}

			$timeText[] = $startsIn->h . $suffix;

		}
		if ($startsIn->i) {

			switch ($startsIn->i) {
				case 1:
					$suffix = " minutu";
					break;
				case 2:
				case 3:
				case 4:
					$suffix = " minuty";
					break;
				default:
					$suffix = " minut";
					break;
			}

			$timeText[] = $startsIn->i . $suffix;
		}

		$timeText = implode(", ", $timeText);
		$movieEntity = $notification->getProjection()->getMovie();
		$cinemaEntity = $notification->getProjection()->getCinema();

		$text = 'Za ' . $timeText . ' začne promítání filmu "' . $movieEntity->getTitle() . '" v kině ' . $cinemaEntity->getName() . '. Příjemnou zábavu ;-)';

		$this->sender->GCMPushMessage("AIzaSyDfEffut7qvzBh0zpaNghZ5NA_MGZ4Qx_8");
		$this->sender->setDevices($notification->getUserId());

		return $this->sender->send(array('message' => $text, 'tickerText' => $text, 'contentTitle' => 'ArtKina: upozornění projekce filmu', 'contentText' => $text));

	}

	/**
	 * @param \GCMPushMessage $sender
	 */
	public function setSender($sender)
	{
		$this->sender = $sender;
	}

	/**
	 * @return \GCMPushMessage
	 */
	public function getSender()
	{
		return $this->sender;
	}


}