<?php

namespace BG\Push;

use App\Doctrine\Entity\Notification;
use Nette\Application\BadRequestException;

class Ios implements IPushSender
{

	/** @var \ApnsPHP_Push $sender */
	protected $sender;

	public function __construct(\ApnsPHP_Push $sender)
	{
		$this->setSender($sender);
	}

	public function send(Notification $notification)
	{
		if ($notification->getType() != Notification::DEVICE_IOS) {
			throw new BadRequestException("Notification is not for iOS device!");
		}

		$projection = $notification->getProjection();
		$startsIn = $notification->getTimestamp()->diff($projection->getDate());

		$timeText = array();
		if ($startsIn->d) {

			switch ($startsIn->d) {
				case 1:
					$suffix = " den";
					break;
				case 2:
				case 3:
				case 4:
					$suffix = " dny";
					break;
				default:
					$suffix = " dnů";
					break;
			}

			$timeText[] = $startsIn->d . $suffix;

		}
		if ($startsIn->h) {

			switch ($startsIn->h) {
				case 1:
					$suffix = " hodinu";
					break;
				case 2:
				case 3:
				case 4:
					$suffix = " hodiny";
					break;
				default:
					$suffix = " hodin";
					break;
			}

			$timeText[] = $startsIn->h . $suffix;

		}
		if ($startsIn->i) {

			switch ($startsIn->i) {
				case 1:
					$suffix = " minutu";
					break;
				case 2:
				case 3:
				case 4:
					$suffix = " minuty";
					break;
				default:
					$suffix = " minut";
					break;
			}

			$timeText[] = $startsIn->i . $suffix;
		}

		$timeText = implode(", ", $timeText);
		$movieEntity = $notification->getProjection()->getMovie();
		$cinemaEntity = $notification->getProjection()->getCinema();

		$text = 'Za ' . $timeText . ' začne promítání filmu "' . $movieEntity->getTitle() . '" v kině ' . $cinemaEntity->getName() . '. Příjemnou zábavu ;-)';

		$sender = $this->getSender();
		$sender->connect();
		$message = new \ApnsPHP_Message($notification->getUserId());
//        truncate text (Push message payload has 256 Bytes limit)
		$message->setText($text);
		$message->setCustomProperty("user_id", $notification->getUserId());
		$message->setCustomIdentifier($notification->getUserId());
		$message->setSound();

		$sender->add($message);
		$sender->send();
		$sender->disconnect();

		return $sender;
	}

	/**
	 * @param \ApnsPHP_Push $sender
	 */
	public function setSender($sender)
	{
		$this->sender = $sender;
	}

	/**
	 * @return \ApnsPHP_Push
	 */
	public function getSender()
	{
		return $this->sender;
	}


}
