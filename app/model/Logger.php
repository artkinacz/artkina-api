<?php

namespace App\Logger;

use Nette\InvalidStateException;
use Nette\Object;
use Nette\Utils\DateTime;

class Logger extends Object
{

	protected $logDir;
	protected $filename;
	protected $filePath;

	const TYPE_ERROR = "ERROR";
	const TYPE_INFO = "INFO";

	const TYPE_CRON_INFO = "CRON:INFO";
	const TYPE_CRON_ERROR = "CRON:ERROR";

	public function __construct($logDir, $filename)
	{
		$this->logDir = $logDir;
		$this->filename = $filename;
		if (!is_dir($logDir)) {
			if (!mkdir($logDir, 0777, TRUE)) {
				throw new InvalidStateException("$logDir cannot be created!");
			}
		}
		$filePath = $logDir . DIRECTORY_SEPARATOR . $filename;
		if (!file_exists($filePath)) {
			touch($filePath);
		}
		$this->filePath = $filePath;
	}

	public function log($message, $type = NULL)
	{
		if (!$type) {
			$type = self::TYPE_INFO;
		}
		$now = new DateTime();
		file_put_contents($this->filePath, "[$now] $type: $message\n", FILE_APPEND);
	}

	/**
	 * @param mixed $filename
	 * @return $this
	 */
	public function setFilename($filename)
	{
		$this->filename = $filename;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getFilename()
	{
		return $this->filename;
	}

	/**
	 * @param mixed $logDir
	 * @return $this
	 */
	public function setLogDir($logDir)
	{
		$this->logDir = $logDir;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getLogDir()
	{
		return $this->logDir;
	}


}