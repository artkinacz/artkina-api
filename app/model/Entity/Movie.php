<?php

namespace App\Doctrine\Entity;

use App\Utils\Mixed;
use BG\Doctrine\Entities\BaseEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;


/**
 * Class Cinema
 * @package App\Doctrine\Entity
 * @ORM\Entity
 * @ORM\Table(name="movie")
 *
 * @property Csfd $csfd
 * @property Tmdb $tmdb
 * @property string $title
 * @property string $titleOrig
 * @property int $csfdStatus
 * @property int $year
 * @property string $description
 * @property string $image
 * @property ArrayCollection $projections
 * @property string $director
 * @property string $country
 */
class Movie extends BaseEntity implements \JsonSerializable
{

	/**
	 * @ORM\OneToOne(targetEntity="Csfd", mappedBy="movie")
	 * @var Csfd
	 */
	protected $csfd;

	/**
	 * @ORM\OneToOne(targetEntity="Tmdb", mappedBy="movie")
	 * @var Tmdb
	 */
	protected $tmdb;


	/**
	 * @ORM\Column(type="string", nullable=TRUE)
	 * @var string
	 */
	protected $title;

	/**
	 * @ORM\Column(type="string", nullable=TRUE)
	 * @var string
	 */
	protected $titleOrig;

	/**
	 * @ORM\Column(type="boolean", nullable=TRUE)
	 * @var integer
	 */
	protected $csfdStatus;

	/**
	 * @ORM\Column(type="integer", nullable=TRUE)
	 * @var integer
	 */
	protected $year;

	/**
	 * @ORM\Column(type="text", nullable=TRUE)
	 * @var string
	 */
	protected $description;

	/**
	 * @ORM\Column(type="string", nullable=TRUE)
	 * @var string
	 */
	protected $image;

	/**
	 * @ORM\OneToMany(targetEntity="Projection", mappedBy="movie", cascade={"persist"})
	 * @var Projection[]|ArrayCollection
	 */
	protected $projections;

	/**
	 * @ORM\Column(type="string", nullable=TRUE)
	 * @var string
	 */
	protected $director;

	/**
	 * @ORM\Column(type="string", nullable=TRUE)
	 * @var string
	 */
	protected $length;

	/**
	 * @ORM\Column(type="string", nullable=TRUE)
	 * @var string
	 */
	protected $country;

	/**
	 * @ORM\Column(type="datetime", nullable=true)
	 * @var datetime
	 */
	protected $tmdb_update;

	/**
	 * @ORM\Column(type="boolean", nullable=true)
	 * @var boolean
	 * TRUE - TMDB data exists
	 * FALSE - TMDB data does not exists or not found. Skip.
	 * NULL - The search hasn't been triggered yet.
	 */
	protected $tmdb_status;


	protected $display;


	function jsonSerialize()
	{
		return $this->toArray();
	}

	public function toArray()
	{
		$vars = get_object_vars($this);
		unset($vars['csfd'], $vars['projections'], $vars['display'], $vars['tmdb'], $vars['tmdb_status'], $vars['tmdb_update']);

		return Mixed::camelToUnderscore($vars);
	}

	public function setTmdbStatus($tmdb_status)
	{
		$this->tmdb_status = $tmdb_status;
		return $this;
	}

	public function getTmdbStatus()
	{
		return $this->tmdb_status;
	}

	public function setTmdbUpdate($tmdb_update)
	{
		$this->tmdb_update = $tmdb_update;
		return $this;
	}

	public function getTmdbUpdate()
	{
		return $this->tmdb_update;
	}

}