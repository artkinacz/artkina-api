<?php

namespace App\Doctrine\Entity;

use App\Utils\Mixed;
use BG\Doctrine\Entities\BaseEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Cinema
 * @package App\Doctrine\Entity
 * @ORM\Entity
 * @ORM\Table(name="cinema")
 *
 * @property string $name
 * @property string $slug
 * @property string $phone
 * @property string $url
 * @property string $email
 * @property string $gps
 * @property string $city
 * @property string $address
 * @property string $zip
 * @property string $district
 * @property bool $active
 * @property ArrayCollection|Projection[] $projections
 *
 */
class Cinema extends BaseEntity implements \JsonSerializable
{
	/**
	 * @ORM\Column(type="string", nullable=FALSE)
	 * @var string
	 */
	protected $name;

	/**
	 * @ORM\Column(type="string", nullable=FALSE)
	 * @var string
	 */
	protected $slug;

	/**
	 * @ORM\Column(type="string", nullable=TRUE)
	 * @var string
	 */
	protected $phone;

	/**
	 * @ORM\Column(type="string", nullable=FALSE)
	 * @var string
	 */
	protected $url;

	/**
	 * @ORM\Column(type="string", nullable=TRUE)
	 * @var string
	 */
	protected $email;

	/**
	 * @ORM\Column(type="string", nullable=TRUE)
	 * @var string
	 */
	protected $gps;

	/**
	 * @ORM\Column(type="string", nullable=FALSE)
	 * @var string
	 */
	protected $city;

	/**
	 * @ORM\Column(type="text", nullable=TRUE)
	 * @var string
	 */
	protected $address;

	/**
	 * @ORM\Column(type="string", length=7, nullable=TRUE)
	 * @var string
	 */
	protected $zip;

	/**
	 * @ORM\Column(type="string", nullable=FALSE)
	 * @var string
	 */
	protected $district;

	/**
	 * @ORM\Column(type="boolean", nullable=FALSE)
	 * @var int
	 */
	protected $active = 0;

	/**
	 * @ORM\OneToMany(targetEntity="Projection", mappedBy="cinema")
	 * @var ArrayCollection
	 */
	protected $projections;

	function jsonSerialize()
	{
		$vars = get_object_vars($this);
		unset($vars['projections']);

		return Mixed::camelToUnderscore($vars);
	}
}