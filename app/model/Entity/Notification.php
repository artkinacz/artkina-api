<?php

namespace App\Doctrine\Entity;

use App\Utils\Mixed;
use BG\Doctrine\Entities\BaseEntity;
use Doctrine\ORM\Mapping as ORM;
use Nette\Utils\DateTime;


/**
 * Class Notificaion
 * @package App\Doctrine\Entity
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 * @ORM\Table(name="notification")
 */
class Notification extends BaseEntity
{

	const DEVICE_IOS = 1;
	const DEVICE_ANDROID = 2;

	public static $types = array(
		"ios" => self::DEVICE_IOS,
		"android" => self::DEVICE_ANDROID
	);

	/**
	 * @ORM\ManyToOne(targetEntity="Projection", inversedBy="notifications", cascade={"persist"})
	 * @ORM\JoinColumn(name="projection_id", referencedColumnName="id")
	 * @var \App\Doctrine\Entity\Projection
	 */
	protected $projection;

	/**
	 * @ORM\Column(type="datetime", nullable=TRUE)
	 * @var DateTime
	 */
	protected $timestamp;

	/**
	 * @ORM\Column(type="text", nullable=FALSE)
	 * @var string
	 */
	protected $userId;

	/**
	 * @ORM\Column(type="datetime", nullable=TRUE)
	 * @var DateTime
	 */
	protected $lastModified;

	/**
	 * @ORM\Column(type="integer", nullable=TRUE)
	 * @var string
	 */
	protected $type;

	/**
	 * @ORM\Column(type="datetime", nullable=TRUE)
	 * @var DateTime
	 */
	protected $sent;


	/**
	 * @ORM\PrePersist
	 * @ORM\PreUpdate
	 */
	public function updatedTimestamps()
	{
		$this->lastModified = new DateTime();
	}

	public function toArray()
	{
		$vars = get_object_vars($this);
		$vars['projection_id'] = $this->projection->getId();
		unset($vars['projection']);

		$vars['date'] = $this->timestamp ? ['date' => $this->timestamp->format("Y-m-d H:i:s")] : NULL;

		return (Mixed::camelToUnderscore($vars));
	}


}
