<?php

namespace App\Doctrine\Entity;

use App\Utils\Mixed;
use BG\Doctrine\Entities\BaseEntity;
use Doctrine\ORM\Mapping as ORM;
use Nette\Utils\DateTime;


/**
 * Class Projection
 * @package App\Doctrine\Entity
 * @ORM\Entity
 * @ORM\Table(name="projection")
 *
 * @property Cinema $cinema
 * @property Movie $movie
 * @property \Datetime $date
 * @property int $price
 * @property string $language
 * @property string $notifications
 * @property bool $enabled
 */
class Projection extends BaseEntity implements \JsonSerializable
{

	/**
	 * @ORM\ManyToOne(targetEntity="Cinema")
	 * @ORM\JoinColumn(name="cinema_id", referencedColumnName="id")
	 **/
	protected $cinema;

	/**
	 * @ORM\ManyToOne(targetEntity="Movie", cascade={"persist"})
	 * @ORM\JoinColumn(name="movie_id", referencedColumnName="id")
	 **/
	protected $movie;

	/**
	 * @ORM\Column(type="datetime", nullable=TRUE)
	 * @var DateTime
	 */
	protected $date;

	/**
	 * @ORM\Column(type="integer", nullable=TRUE)
	 * @var integer
	 */
	protected $price;

	/**
	 * @ORM\Column(type="string", nullable=TRUE)
	 * @var string
	 */
	protected $language;

	/**
	 * @ORM\OneToMany(targetEntity="Notification", mappedBy="projection", cascade={"persist"})
	 * @var Notification
	 */
	protected $notifications;

	/**
	 * @ORM\Column(type="boolean")
	 * @var bool
	 */
	protected $enabled = TRUE;

	public function toArray()
	{
		$vars = get_object_vars($this);
		unset($vars['notifications']);
		$vars['movie_id'] = $this->movie->getId();
		$vars['cinema_id'] = $this->cinema->getId();
		unset($vars['cinema'], $vars['movie']);

		$vars['date'] = $this->date ? ['date' => $this->date->format("Y-m-d H:i:s")] : NULL;

		return Mixed::camelToUnderscore($vars);
	}


	/**
	 * (PHP 5 &gt;= 5.4.0)<br/>
	 * Specify data which should be serialized to JSON
	 * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
	 * @return mixed data which can be serialized by <b>json_encode</b>,
	 * which is a value of any type other than a resource.
	 */
	function jsonSerialize()
	{
		return $this->toArray();
	}
}