<?php

namespace App\Doctrine\Entity;

use App\Utils\Mixed;
use BG\Doctrine\Entities\BaseEntity;
use Doctrine\ORM\Mapping as ORM;


/**
 * Class Tmdb
 * @package App\Doctrine\Entity
 * @ORM\Entity
 * @ORM\Table(name="tmdb")
 */
class Tmdb extends BaseEntity
{

	/**
	 * @ORM\Column(type="integer", nullable=FALSE)
	 * @var integer
	 */
	protected $tmdbId;

	/**
	 * @ORM\Column(type="boolean", nullable=TRUE)
	 * @var boolean
	 */
	protected $adult;

	/**
	 * @ORM\Column(type="string", nullable=TRUE)
	 * @var string
	 */
	protected $backdrop_path;

	/**
	 * @ORM\Column(type="string", nullable=TRUE)
	 * @var string
	 */
	protected $poster_path;

	/**
	 * @ORM\Column(type="string", nullable=TRUE)
	 * @var string
	 */
	protected $original_language;

	/**
	 * @ORM\Column(type="string", nullable=FALSE)
	 * @var string
	 */
	protected $title;

	/**
	 * @ORM\Column(type="string", nullable=TRUE)
	 * @var string
	 */
	protected $original_title;

	/**
	 * @ORM\Column(type="text", nullable=TRUE)
	 * @var text
	 */
	protected $overview;

	/**
	 * @ORM\Column(type="string", nullable=TRUE)
	 * @var string
	 */
	protected $genre_ids;

	/**
	 * @ORM\Column(type="date", nullable=TRUE)
	 * @var date
	 */
	protected $release_date;

	/**
	 * @ORM\Column(type="float", nullable=TRUE)
	 * @var float
	 */
	protected $popularity;

	/**
	 * @ORM\Column(type="float", nullable=TRUE)
	 * @var float
	 */
	protected $vote_average;

	/**
	 * @ORM\Column(type="integer", nullable=TRUE)
	 * @var integer
	 */
	protected $vote_count;

	/**
	 * @ORM\Column(type="string", nullable=TRUE)
	 * @var string
	 */
	protected $imdb_id;

	/**
	 * @ORM\Column(type="float", nullable=TRUE)
	 * @var floar
	 */
	protected $imdb_rating;

	/**
	 * @ORM\Column(type="string", nullable=TRUE)
	 * @var string
	 */
	protected $youtube;

	/**
	 * @ORM\OneToOne(targetEntity="Movie", inversedBy="tmdb", cascade={"persist"})
	 * @ORM\JoinColumn(name="movie_id", referencedColumnName="id")
	 * @var Movie
	 */
	protected $movie;

	public function toArray()
	{
		$vars = get_object_vars($this);
		unset($vars['movie']);
		return Mixed::camelToUnderscore($vars);
	}

	public function setBackdropPath($backdrop_path)
	{
		$this->backdrop_path = $backdrop_path;
		return $this;
	}

	public function getBackdropPath()
	{
		return $this->backdrop_path;
	}

	public function setPosterPath($poster_path)
	{
		$this->poster_path = $poster_path;
		return $this;
	}

	public function getPosterPath()
	{
		return $this->poster_path;
	}

	public function setOriginalLanguage($original_language)
	{
		$this->original_language = $original_language;
		return $this;
	}

	public function getOriginalLanguage()
	{
		return $this->original_title;
	}

	public function setOriginalTitle($original_title)
	{
		$this->original_title = $original_title;
		return $this;
	}

	public function getOriginalTitle()
	{
		return $this->original_title;
	}

	public function setGenreIds($genre_ids)
	{
		$this->genre_ids = $genre_ids;
		return $this;
	}

	public function getGenreIds()
	{
		return $this->genre_ids;
	}

	public function setReleaseDate($release_date)
	{
		$this->release_date = $release_date;
		return $this;
	}

	public function getReleaseDate()
	{
		return $this->release_date;
	}

	public function setVoteAverage($vote_average)
	{
		$this->vote_average = $vote_average;
		return $this;
	}

	public function getVoteAverage()
	{
		return $this->vote_average;
	}

	public function setVoteCount($vote_count)
	{
		$this->vote_count = $vote_count;
		return $this;
	}

	public function getVoteCount()
	{
		return $this->vote_count;
	}

	public function setImdbId($imdb_id)
	{
		$this->imdb_id = $imdb_id;
		return $this;
	}

	public function getImdbId()
	{
		return $this->imdb_id;
	}

	public function setImdbRating($imdb_rating)
	{
		$this->imdb_rating = $imdb_rating;
		return $this;
	}

	public function getImdbRating()
	{
		return $this->imdb_rating;
	}
}