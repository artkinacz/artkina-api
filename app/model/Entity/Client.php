<?php


namespace App\Doctrine\Entity;


use Doctrine\ORM\Mapping as ORM;
use Kdyby\Doctrine\Entities\BaseEntity;

/**
 * Class UUID
 * @package App\Doctrine\Entity
 *
 * @ORM\Entity()
 * @ORM\Table()
 */
class Client extends BaseEntity
{
	/**
	 * @ORM\Id()
	 * @ORM\Column()
	 * @var string
	 */
	protected $uuid;

	/**
	 * @ORM\Column(type="datetime")
	 * @var \DateTime
	 */
	protected $date;

	public function __construct($uuid)
	{
		$this->uuid = $uuid;
		$this->date = new \DateTime();
	}


} 