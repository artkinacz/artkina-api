<?php

namespace BG\Doctrine\Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * @author Jaromír Navara <web@bagricek.cz>
 * @ORM\MappedSuperclass()
 * @property-read int $id
 */
abstract class BaseEntity extends \Kdyby\Doctrine\Entities\BaseEntity
{
	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue
	 * @var integer
	 */
	protected $id;

	/**
	 * @return integer
	 */
	final public function getId()
	{
		return $this->id;
	}

	public function __clone()
	{
		$this->id = NULL;
	}

}
