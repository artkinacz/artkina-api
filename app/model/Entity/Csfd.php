<?php

namespace App\Doctrine\Entity;

use App\Utils\Mixed;
use BG\Doctrine\Entities\BaseEntity;
use Doctrine\ORM\Mapping as ORM;


/**
 * Class Cinema
 * @package App\Doctrine\Entity
 * @ORM\Entity
 * @ORM\Table(name="csfd")
 */
class Csfd extends BaseEntity
{

	/**
	 * @ORM\Column(type="integer", nullable=FALSE)
	 * @var int
	 */
	protected $csfdId;

	/**
	 * @ORM\Column(type="integer", nullable=TRUE)
	 * @var integer
	 */
	protected $year;

	/**
	 * @ORM\Column(type="string", nullable=TRUE)
	 * @var string
	 */
	protected $posterUrl;

	/**
	 * @ORM\Column(type="string", nullable=TRUE)
	 * @var integer
	 */
	protected $title_cs;

	/**
	 * @ORM\Column(type="string", nullable=TRUE)
	 * @var integer
	 */
	protected $title_sk;

	/**
	 * @ORM\Column(type="string", nullable=TRUE)
	 * @var integer
	 */
	protected $title_en;

	/**
	 * @ORM\Column(type="string", nullable=TRUE)
	 * @var integer
	 */
	protected $genres;

	/**
	 * @ORM\Column(type="string", nullable=TRUE)
	 * @var integer
	 */
	protected $countries;

	/**
	 * @ORM\Column(type="text", nullable=TRUE)
	 * @var string
	 */
	protected $description;

	/**
	 * @ORM\Column(type="string", nullable=TRUE)
	 * @var integer
	 */
	protected $accessibility;

	/**
	 * @ORM\Column(type="string", nullable=TRUE)
	 * @var integer
	 */
	protected $length;

	/**
	 * @ORM\Column(type="integer", nullable=TRUE)
	 * @var integer
	 */
	protected $rating;

	/**
	 * @ORM\Column(type="string", nullable=TRUE)
	 * @var string
	 */
	protected $director;

	/**
	 * @ORM\OneToOne(targetEntity="Movie", inversedBy="csfd", cascade={"persist"})
	 * @ORM\JoinColumn(name="movie_id", referencedColumnName="id")
	 * @var Movie
	 */
	protected $movie;

	public function toArray()
	{
		$vars = get_object_vars($this);
		unset($vars['movie']);

		return Mixed::camelToUnderscore($vars);
	}

	/**
	 * @param int $title_cs
	 */
	public function setTitleCs($title_cs)
	{
		$this->title_cs = $title_cs;

		return $this;
	}

	/**
	 * @param int $title_en
	 */
	public function setTitleEn($title_en)
	{
		$this->title_en = $title_en;

		return $this;
	}

	/**
	 * @param int $title_sk
	 */
	public function setTitleSk($title_sk)
	{
		$this->title_sk = $title_sk;

		return $this;
	}

	/**
	 * @return int
	 */
	public function getTitleCs()
	{
		return $this->title_cs;
	}

	/**
	 * @return int
	 */
	public function getTitleEn()
	{
		return $this->title_en;
	}

	/**
	 * @return int
	 */
	public function getTitleSk()
	{
		return $this->title_sk;
	}

	public function getLink()
	{
		return $this->csfdId ? 'http://www.csfd.cz/film/'.$this->csfdId : NULL;
	}


}