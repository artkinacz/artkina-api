<?php
namespace Parser;

final class ImdbParser
{

	const API_URL = 'http://www.omdbapi.com';
	const SLEEP_INTERVAL = 1;

	public function getMovie($ttId)
	{
		sleep(self::SLEEP_INTERVAL);

		$url = self::API_URL . '/?' . http_build_query(array(
				'i'   => $ttId
		));
		$movie = json_decode(file_get_contents($url));

		return $movie;
	}

}