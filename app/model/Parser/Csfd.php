<?php
namespace Parser;

final class Csfd
{

	const API_MOVIES_URL = 'http://csfdapi.cz/movie';

	public function findMovies($title)
	{

		sleep(5);

		$url = self::API_MOVIES_URL . '?' . http_build_query(array(
				'search' => $title,
			));
		$movies = json_decode(file_get_contents($url));

		return $movies;

	}

	public function getMovie($movieId)
	{

		sleep(1);

		$url = self::API_MOVIES_URL . '/' . $movieId;
		$movie = json_decode(file_get_contents($url));

		return $movie;

	}

}