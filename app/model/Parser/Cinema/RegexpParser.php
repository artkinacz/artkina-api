<?php
namespace Parser\Cinema;

use DateTime;
use Nette\Utils\ArrayHash;
use Parser\Entity\Movie;
use Parser\Entity\Projection;

/**
 * Base paresr class using regular expressions. Suitable as a parent class for
 * cinemas Aero, BIO|OKO and Svetozor.
 *
 * @author Jakub Fiser <jakubfiser@jakubfiser.com>
 */
abstract class RegexpParser implements IParser
{
	/**
	 * Content loader.
	 */
	protected $loader;

	/**
	 * Content of parsed document.
	 */
	protected $content;

	/**
	 * Array of movies found in parsed document (no duplicites are created).
	 */
	protected $movies = array();

	/**
	 * Array of projections found in parsed document.
	 */
	protected $projections = array();

	/**
	 * Array of image links corresponding to the found movies.
	 */
	protected $images = array();

	/**
	 * Creates the instance of the parser and parses the document placed on given URL.
	 *
	 * @param string $url URL of the content to be parsed.
	 */
	public function __construct($url)
	{
		$this->loader = new ContentLoader;
		$this->content = $this->loader->getHtml($url);

		$this->loadMovies();
		$this->loadProjections();
	}

	/**
	 * Returns the array with the program of current day.
	 *
	 * @param string $day Day in 'YYYY-MM-DD' format.
	 * @return array
	 */
	public function parseDay($day)
	{
		if (!isset($this->projections[$day])) {
			return array();
		}

		$result = array();

		foreach ($this->projections[$day] as $projection) {
			if (isset($this->images[$projection->link])) {
				$projection->image_url = $this->images[$projection->link];
			} else {
				$projection->image_url = $this->images[$projection->link] = $this->loadImage($projection->link);
			}
			$movieEntity = new Movie();
			$projectionEntity = new Projection();

			$record = clone $this->movies[$projection->movie];
			$movieEntity->setTitle($record->title);
			$movieEntity->setTitleOrig($record->title_orig);
			$movieEntity->setYear($record->year);
			$movieEntity->setDescription($record->description);
			$movieEntity->setDirector($record->director);
			$projectionEntity->setLanguage($record->language);
			$projectionEntity->setDate(new DateTime($projection->date));
			$projectionEntity->setPrice($projection->price);
			$movieEntity->setImage($projection->image_url);
			$movieEntity->setProjection($projectionEntity);
			$result[] = $movieEntity;
		}

		return array(
			$day => $result,
		);
	}

	/**
	 * Just an wrapper method for parseDay
	 * @param DateTime $day
	 * @return ArrayHash
	 */
	public function getDay(DateTime $day)
	{
		$data = $this->parseDay($day->format("Y-m-d"));

		return array_pop($data);
	}

	/**
	 * Parses the content of the document and finds and processes the projections.
	 */
	protected function loadProjections()
	{
		preg_match_all('/(?<tags>\<item rdf:about=[\S\s]+\<\/kult:Price\>)/U', $this->content, $matches);

		foreach ($matches['tags'] as $tag) {
			preg_match('/\<link rdf:datatype[\S\s]+\>(?<link>[\S\s]+)\<\/link\>' .
				'[\S\s]*\<ev:startdate rdf:datatype[\S\s]+\>(?<startdate>[\S\s]+)\<\/ev:startdate\>' .
				'[\S\s]*\<kult:hasEvent rdf:resource=\"(?<movie_uri>[\S\s]+)\"\/>' .
				'[\S\s]*\<kult:value rdf:datatype[\S\s]+\>(?<price>[\S\s]+)\<\/kult:value\>' .
				'/U', $tag, $tagMatches
			);

			if (isset($this->movies[$tagMatches['movie_uri']])) {
				$date = new DateTime($tagMatches['startdate']);
				$dateKey = $date->format('Y-m-d');

				if (!isset($this->projections[$dateKey])) {
					$this->projections[$dateKey] = array();
				}

				$projection = new ArrayHash();
				$projection->date = $date->format('Y-m-d H:i');
				$projection->movie = $tagMatches['movie_uri'];
				$projection->price = (int)$tagMatches['price'];
				$projection->link = $tagMatches['link'];

				$this->projections[$dateKey][] = $projection;
			}
		}
	}

	/**
	 * Parses the content of the document and finds and processes the movies.
	 */
	protected function loadMovies()
	{
		preg_match_all('/(?<tags>\<kult:Movie rdf:about=[\S\s]+\<\/kult:Movie\>)/U', $this->content, $matches);

		foreach ($matches['tags'] as $tag) {
			preg_match('/^\<kult:Movie rdf:about=\"(?<uri>[\S\s]+)\"\>' .
				'[\S\s]*\<kult:movie_title xml:lang=\"cs\"\>(?<title_cs>[\S\s]+)\<\/kult:movie_title\>' .
				'[\S\s]*\<kult:movie_originalTitle[\S\s]+\>(?<title_orig>[\S\s]+)\<\/kult:movie_originalTitle\>' .
				'[\S\s]*\<kult:movie_year[\S\s]+\>(?<year>[\S\s]+)\<\/kult:movie_year\>' .
				'[\S\s]*\<kult:movie_description xml:lang=\"cs\"\>(?<description>[\S\s]+)\<\/kult:movie_description\>' .
				'/U', $tag, $tagMatches
			);

			preg_match('/[\S\s]*\<kult:movie_subtitlesLanguage[\S\s]+\>(?<language>[\S\s]+)\<\/kult:movie_subtitlesLanguage\>/U',
				$tag, $tagMatches3);
			preg_match('/[\S\s]*\<kult:movie_hasDirector[\S\s]+\>(?<director>[\S\s]+)\<\/kult:movie_hasDirector\>/U',
				$tag, $tagMatches2);

			$tagMatches = array_merge($tagMatches, $tagMatches2, $tagMatches3);
			$movie = new ArrayHash();
			$movie->cinema = $this->getName();
			$movie->title = $tagMatches['title_cs'];
			$movie->title_orig = $tagMatches['title_orig'];
			$movie->year = (0 === (int)$tagMatches['year']) ? NULL : (int)$tagMatches['year'];
			$movie->description = $this->cleanDescription($tagMatches['description']);
			$movie->director = isset($tagMatches['director']) ? $tagMatches['director'] : NULL;
			$movie->language = isset($tagMatches['language']) ? $tagMatches['language'] : NULL;
			$this->movies[$tagMatches['uri']] = $movie;
		}
//        die();
	}

	/**
	 * Cleans the description text.
	 *
	 * @param string $description
	 * @return string
	 */
	protected function cleanDescription($description)
	{
		$description = preg_replace('/^\<\!\[CDATA\[/u', '', $description);
		$description = preg_replace('/\]\]\>$/u', '', $description);
		$description = preg_replace('/\s/u', ' ', $description);
		$description = preg_replace('/  +/u', ' ', $description);

		return trim($description);
	}

	/**
	 * Transforms the URL. Only for polymorphism purposes.
	 *
	 * @param string $url
	 * @return string
	 */
	protected function setupUrl($url)
	{
		return $url;
	}

	/**
	 * Parses the movie page (obtained from parsing the basic document) and finds
	 * the link to the movie's image.
	 *
	 * @param string $url
	 * @return string
	 */
	protected function loadImage($url)
	{
		$url = $this->setupUrl($url);

		if (isset($this->images[$url])) {
			return $this->images[$url];
		}

		$content = $this->loader->getHtml($url);

		preg_match($this->getImageRegexp(), $content, $matches);

		return (isset($matches['url']) && !empty($matches['url'])) ? $this->setupUrl($matches['url']) : NULL;
	}

	/**
	 * Returns the name of the parser.
	 *
	 * @return string
	 */
	public function getName()
	{
		return NULL;
	}

	public abstract function getImageRegexp();
}
