<?php
namespace Parser\Cinema;

use Nette\Utils\DateTime;
use Parser\Entity\Movie;
use Parser\Entity\Projection;
use Sunra\PhpSimple\HtmlDomParser;

class ArtKinoParser implements IParser
{

	const PROGRAM_URL = "http://kinoart.cz/program/";
	const BASE_URL = "http://kinoart.cz/";

	/**
	 * Fetches array of movies played on $day
	 * @param DateTime $day
	 * @return array
	 */
	public function getDay(DateTime $day)
	{
		return $this->getDayProgram($day);
	}


	/**
	 * @param $url
	 * @param DateTime $day
	 * @return Movie
	 */
	protected function getMovieDetail($url, DateTime $day)
	{
		$html = HtmlDomParser::file_get_html($url);
		$mainContent = $html->find("#content", 0);
		$when = $day;

//        parse movie detail
		$leftCol = $mainContent->find(".leftcol", 0);
		$perex = $leftCol->find("p", 1)->plaintext;
		$perex = explode("|", $perex);

//        naplneni filmu
		$movie = new Movie();
		$movie->setTitle($mainContent->find("h1", 0)->plaintext);
		$movie->setTitleOrig(isset($perex[0]) ? trim($perex[0]) : NULL);
		$movie->setYear(isset($perex[1]) ? trim($perex[1]) : NULL);
		$movie->setDirector(isset($perex[2]) ? trim($perex[2]) : NULL);
		$movie->setCountry(isset($perex[3]) ? trim($perex[3]) : NULL);
		$movie->setImage($leftCol->find("img", 0)->src);
		$movie->setDescription($leftCol->find("p", 2)->plaintext);

		$program = $mainContent->find(".rightcol .program", 0);

		foreach ($program->find("tr") as $tr) {
			$boxDate = $tr->find(".date", 0);
			$boxDate = strip_tags($boxDate);
			$boxDate = preg_replace("/^[^0-9]+/", "", $boxDate);
			$date = explode(".", $boxDate);
			$day = $date[0];
			$month = $date[1];
			if (trim("$day $month") !== $when->format("j n")) {
				continue;
			}
			$time = explode(":", $date[2]);
			$projectionStart = new DateTime();
			$projectionStart->setDate($projectionStart->format("Y"), $month, $day);
			$projectionStart->setTime($time[0], $time[1], 0);
			$price = $tr->find(".price", 0)->plaintext;
			preg_match("/[0-9]+/", $price, $price);

//            naplneni projekce
			$projection = new Projection();
			$projection->setDate($projectionStart);
			$projection->setPrice(!empty($price) ? $price[0] : 0);
			$projection->setLanguage($tr->find(".sub", 0)->plaintext);
			$movie->setProjection($projection);
		}

		return $movie;
	}

	/**
	 * @param DateTime $day
	 * @return array
	 */
	protected function getDayProgram(DateTime $day)
	{
		$when = $day;
		$html = HtmlDomParser::file_get_html(self::PROGRAM_URL);
		$movies = [];
		$mainNode = $html->find(".program tr");
		if (empty($mainNode)) {
			throw new ParserException("Unable to find root node in " . get_class($this));
		}

		foreach ($mainNode as $movie) {
			$boxDate = $movie->find(".date", 0);
			$boxDate = strip_tags($boxDate);
			$boxDate = preg_replace("/^[^0-9]+/", "", $boxDate);
			$date = explode(".", $boxDate);
			$day = $date[0];
			$month = $date[1];
			if (trim("$day $month") !== $when->format("j n")) {
				continue;
			}

			$link = html_entity_decode($movie->find(".movie a", 0)->href);
			$movies[] = $this->getMovieDetail($link, $when);
		}

		return $movies;
	}


	function cesky_mesic($mesic)
	{
		$mesic = (int)$mesic;
		static $nazvy = array(1 => 'leden', 'únor', 'březen', 'duben', 'květen', 'červen', 'červenec', 'srpen', 'září', 'říjen', 'listopad', 'prosinec');

		return $nazvy[$mesic];
	}
}