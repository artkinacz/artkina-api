<?php
namespace Parser\Cinema;

/**
 * Regular expression parser parsing cinema Aero's program.
 *
 * @author Jakub Fiser <jakubfiser@jakubfiser.com>
 */
final class AeroRegexpParser extends RegexpParser
{
	/**
	 * Parser/cinema name.
	 */
	const NAME = 'Aero';

	/**
	 * URL of parsed document.
	 */
	const PROGRAM_URL = 'http://www.kinoaero.cz/export/';

	/**
	 * Creates the instance of the parser.
	 */
	public function __construct()
	{
		parent::__construct(self::PROGRAM_URL);
	}

	/**
	 * Returns the regular expression to be used for extract image link.
	 *
	 * @return string
	 */
	public function getImageRegexp()
	{
		return '/<p class=\"movie-image\"[\S\s]+<img src=\"(?<url>[\S\s]+)\"/U';
	}

	/**
	 * Returns the parser/cinema name.
	 *
	 * @return string
	 */
	public function getName()
	{
		return self::NAME;
	}
}
