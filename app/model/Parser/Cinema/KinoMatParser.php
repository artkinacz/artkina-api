<?php
namespace Parser\Cinema;

use DateTime;
use Nette\Http\Url;
use Parser\Entity\Movie;
use Parser\Entity\Projection;
use Sunra\PhpSimple\HtmlDomParser;

class KinoMatParser implements IParser
{
	const PROGRAM_URL = "http://www.mat.cz/matclub/cz/kino/mesicni-program";
	const BASE_URL = "http://www.mat.cz/";

	/**
	 * Fetches array of movies played on $day
	 * @param DateTime $day
	 * @return \App\Doctrine\Entity\Movie[]
	 */
	public function getDay(DateTime $day)
	{
		$html = HtmlDomParser::file_get_html(self::PROGRAM_URL);

		$content = $html->find('.content_kalendarcon', 0);

		$calendar = NULL;
		foreach ($content->find(".kalendar") as $calendar) {
			$date = implode("", array_slice(explode(" ", $calendar->innertext), -2));

			if ($date == $day->format("j.n.")) {
				break;
			}
		}
		$films = $calendar->next_sibling();
		$movies = [];
		$mailContent = $films->find('.film');
		if (empty($mailContent)) {
			throw new ParserException("Unable to find root node in " . get_class($this));
		}
		foreach ($mailContent as $film) {
			$movies[] = $this->parseMovie($film, $day);
		}

		return $movies;
	}

	private function parseMovie(\simple_html_dom_node $film, \DateTime $day)
	{
		$movie = new Movie();
		$projection = new Projection();
		$projection->setMovie($movie);
		$movie->projection = $projection;
		$day = clone $day;

		$timeBlock = $this->sanitize($film->find('.film1', 0)->plaintext);
		$time = explode(".", $timeBlock);
		if (empty($time)) {
			return;
		}
		$day->setTime($time[0], $time[1]);
		$projection->setDate($day);

		$mainBlock = $film->find('.film2', 0);
		$link = $mainBlock->find('a', 0);
		if (!$link) {
			return NULL;
		}

		$movie->setTitle($this->sanitize($link->plaintext));
		$movie->setTitleOrig($this->sanitize($mainBlock->find('.film2h5', 0)->plaintext));

		$movie->setLength((int)$this->sanitize($film->find('.film3', 0)->plaintext));
		$projection->setLanguage($this->sanitize($film->find('.film4', 0)->plaintext));
		$projection->setPrice((int)$this->sanitize($film->find('.film5', 0)->plaintext));

		$this->parseMovieDetail($link->href, $movie);

		return $movie;
	}

	private function parseMovieDetail($href, Movie $movie)
	{
		/** @var \simple_html_dom $html */
		$html = HtmlDomParser::file_get_html($this->getUrl(self::PROGRAM_URL, $href));
		/** @var \simple_html_dom_node $mainBlock */
		$mainBlock = $html->find('.content', 0);

		/** @var \simple_html_dom_node $details */
		$details = $mainBlock->find('.kinodetailheader', 0)->next_sibling();

		if ($details->tag === 'p') {
			$content = explode("|", $details->plaintext);

			array_walk($content, function (&$item) {
				$item = $this->sanitize($item);
			});

			$movie->setCountry(array_shift($content));
			$movie->setYear(array_shift($content));
		}

		$annot = $mainBlock->find('.dvddetail2', 0);
		$text = [];
		foreach ($annot->find('p') as $p) {
			$text[] = $this->sanitize($p->plaintext);
		}
		$movie->setDescription(implode("\n", $text));

	}

	private function getUrl($program, $requested)
	{
		$url = new Url($program);
		$url->setPath($requested);

		return $url->getAbsoluteUrl();
	}

	private function sanitize($value)
	{
		$value = $this->decodeAccents($value);

		return trim(preg_replace("/&#?[a-z0-9]+;/i", "", strip_tags($value)));
	}

	private function decodeAccents($value)
	{

		return preg_replace_callback('/&\w(acute|uml|tilde|caron|circ);/', function ($m) {
			return html_entity_decode($m[0], ENT_QUOTES, "UTF-8");
		}, $value
		);
	}
}