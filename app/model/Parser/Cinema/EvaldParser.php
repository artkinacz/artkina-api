<?php
namespace Parser\Cinema;

use App\Parser\Generic\CinemawareCrawler;

class EvaldParser extends CinemawareCrawler
{
	const PROGRAM_URL = "http://www.evald.cz/";
	const BASE_URL = "http://www.evald.cz/";

	function __construct()
	{
		parent::__construct(self::BASE_URL, self::PROGRAM_URL);
	}


}