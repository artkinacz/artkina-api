<?php
namespace Parser\Cinema;

/**
 * Regular expression parser parsing cinema Aero's program.
 *
 * @author Jakub Fiser <jakubfiser@jakubfiser.com>
 */
final class SvetozorRegexpParser extends RegexpParser
{
	/**
	 * Parser/cinema name.
	 */
	const NAME = 'Světozor';

	/**
	 * URL of parsed document.
	 */
	const PROGRAM_URL = 'http://www.kinosvetozor.cz/export/';

	/**
	 * Cinema's base URL.
	 */
	const BASE_URL = 'http://www.kinosvetozor.cz';

	/**
	 * Creates the instance of the parser.
	 */
	public function __construct()
	{
		parent::__construct(self::PROGRAM_URL);
	}

	/**
	 * 'Absolutizes' the given URL.
	 *
	 * @param string $url
	 * @return string
	 */
	protected function setupUrl($url)
	{
		return self::BASE_URL . $url;
	}

	/**
	 * Returns the regular expression to be used for extract image link.
	 *
	 * @return string
	 */
	public function getImageRegexp()
	{
		return '/<div class=\"imageSide\"[\S\s]+<img src=\"(?<url>[\S\s]+)\"/U';
	}

	/**
	 * Returns the parser/cinema name.
	 *
	 * @return string
	 */
	public function getName()
	{
		return self::NAME;
	}
}
