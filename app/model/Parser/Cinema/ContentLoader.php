<?php
namespace Parser\Cinema;

/**
 * Very simple loader of HTML content placed on given URL.
 *
 * @author Jakub Fiser <jakubfiser@jakubfiser.com>
 */
class ContentLoader
{
	/**
	 * Loads the HTML content on given URL address.
	 *
	 * @param  string $url Url of HTML page to load.
	 * @return string
	 */
	public function getHtml($url)
	{
		if (ini_get('allow_url_fopen') === '1') {
			$html = file_get_contents($url);
		} else {
			$curl = curl_init();
			$timeout = 8;
			curl_setopt($curl, CURLOPT_URL, $url);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, $timeout);
			$html = curl_exec($curl);
			curl_close($curl);
		}

		if ($html === FALSE) {
			throw new \Exception("Can't load the content of '$url'.");
		}

		return $html;
	}

	public function getImage($url, $path)
	{
		$error = FALSE;
		try {
			if (ini_get('allow_url_fopen') === '1') {
				$res = @file_put_contents($path, file_get_contents($url));
				$error = ($res === FALSE);
			} else {
				$curl = curl_init();
				$timeout = 8;
				curl_setopt($curl, CURLOPT_URL, $url);
				curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, $timeout);
				curl_setopt($curl, CURLOPT_FILE, $path);
				curl_setopt($curl, CURLOPT_HEADER, 0);
				$res = curl_exec($curl);
				$error = ($res === FALSE);
				curl_close($curl);
				fclose($path);
			}

		} catch (\Exception $e) {
			$error = TRUE;
		}

		if ($error) {
			throw new \Exception("Nelze stahnout obrazek z adresy '$url'.");
		}
	}
}

