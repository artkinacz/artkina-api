<?php


namespace Parser\Cinema;


use Kdyby\Curl\CurlWrapper;
use Kdyby\CurlCaBundle\CertificateHelper;
use Nette\Utils\DateTime;
use Parser\Entity\Movie;
use Parser\Entity\Projection;

class KinoLucernaParser implements IPeriodAwareParser
{

	private $endpoint = 'https://system.cinemaware.eu/dr2_predstavenia_za_obdobie_xml.php?xml=true&id=68&predstavenia_od=%since%&predstavenia_do=%to%';

	/** @inheritdoc */
	function getProgram(\DateTime $since, $period)
	{

		$structure = $this->parseData($since, $period);

		$result = [];

		foreach ($structure as $movie) {
			$key = $movie->projection->date->format("Y-m-d");
			if (!isset($result[$key])) {
				$result[$key] = [];
			}
			$result[$key][] = $movie;
		}

		return $result;
	}

	private function parseData(\DateTime $since, $period)
	{
		$to = clone $since;
		$to->modify("+ $period days");

		$url = str_replace('%since%', $since->format("Y-m-d"), $this->endpoint);
		$url = str_replace('%to%', $to->format("Y-m-d"), $url);
		$data = $this->download($url);

		if (!$data) {
			throw new ParserException("No data found.");
		}

		$tree = simplexml_load_string($data);

		$movies = [];

		foreach ($tree->filmy->film as $item) {
			$movie = new Movie();

			$movie->title = $this->sanitize($item->nazov);
			$movie->titleOrig = $this->sanitize($item->nazov_originalny);
			if (isset($item->rok_vyroby)) {
				$movie->year = (int)$this->sanitize($item->rok_vyroby) ?: NULL;
			}
			if (isset($item->dlzka)) {
				$movie->setLength(((int)$this->sanitize($item->dlzka)) ?: NULL);
			}
			if (isset($item->krajiny_povodu)) {
				$movie->setCountry($this->sanitize($item->krajiny_povodu->krajina['kod']));
			}

			if (isset($item->popis)) {
				$movie->setDescription(trim($item->popis));
			}
			if (isset($item->prilohy->plagat->velky)) {
				$movie->setImage($this->sanitize($item->prilohy->plagat->velky['url']));
			}

			foreach ($item->predstavenia->predstavenie as $proj) {
				$m = clone $movie;
				$projection = new Projection();
				$projection->setDate(new DateTime($proj->zaciatok['datum'] . ' ' . $proj->zaciatok['cas']));
				if (isset($proj->ceny->zakladna)) {
					$projection->setPrice($this->sanitize($proj->ceny->zakladna));
				}
				$projection->setLanguage($this->sanitize($proj->verzia_nazov_skratka));

				$m->setProjection($projection);
				$projection->setMovie($m);
				$movies[] = $m;
			}

		}

		return $movies;


	}

	protected function download($url)
	{
		$curl = new CurlWrapper($url);

		$curl->setOption('cainfo', CertificateHelper::getCaInfoFile());


		$curl->execute();

		$response = $curl->response;

		return $response;
	}

	private function sanitize($value)
	{
		return trim(preg_replace("/&#?[a-z0-9]+;/i", "", strip_tags($value)));
	}

}