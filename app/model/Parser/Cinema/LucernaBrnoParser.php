<?php
namespace Parser\Cinema;

use DateTime;
use Parser\Entity\Movie;
use Parser\Entity\Projection;
use Sunra\PhpSimple\HtmlDomParser;

class LucernaBrnoParser implements IParser
{

	const PROGRAM_URL = "http://www.kinolucerna.info/index.php/program";
	const BASE_URL = "http://www.kinolucerna.info";

	/**
	 * Fetches array of movies played on $day
	 * @param DateTime $day
	 * @return array
	 */
	public function getDay(DateTime $day)
	{
		return $this->getDayProgram($day);
	}

	protected function getMovieDetail($url, DateTime $day)
	{
		$html = HtmlDomParser::file_get_html(self::BASE_URL . $url);
		$mainContent = $html->find("#icagenda", 0);

//        parse movie detail
		$movie = new Movie();
		$projection = new Projection();

//        movie title
		$title = $mainContent->find("h1", 0)->plaintext;
		$title = explode("/", $title);
		if (count($title) > 1) {
			$movie->setTitleOrig(trim($title[1]));
			$movie->setTitle(trim($title[0]));
		} else {
			$movie->setTitle(trim($title[0]));
		}

		if (!$movie->getTitle()) {
			return;
		}

//        time
		$time = $mainContent->find(".evttime", 0)->plaintext;
		$time = explode(":", $time);
		$day->setTime(trim($time[0]), trim($time[1]));
		$projection->setDate($day);

//        description, price, director

		$infoBox = $mainContent->find("#detail-desc p", 0);
		if (!$infoBox->find("strong", 0)) {
			$infoBox = $mainContent->find("#detail-desc p", 1);
		}
		if (!$infoBox->find("strong", 0)) {
			$infoBox = $mainContent->find("#detail-desc p", 2);
		}

		$movie->setProjection($projection);
		if (!$infoBox) {
			return $movie;
		}
		$projection->setPrice(substr($infoBox->find("strong", 0)->plaintext, 8));
		$infoBox = explode("<br />", $infoBox->innertext);
		unset($infoBox[0]);
		$movie->setDescription(implode("\n", $infoBox));

		return $movie;
	}

	/**
	 * @param DateTime $day
	 * @return array
	 */
	protected function getDayProgram(DateTime $day)
	{
		$when = $day;
		$html = HtmlDomParser::file_get_html(self::PROGRAM_URL);
		$movies = [];
		setlocale(LC_TIME, "czech");
		$mainNode = $html->find(".event");
		if (empty($mainNode)) {
			throw new ParserException("Unable to find root node in " . get_class($this));
		}
		foreach ($mainNode as $movie) {
			$dayFormatted = $when->format("d") . "  " . $this->cesky_mesic($when->format("m"));
			$projectionStart = trim($movie->find(".evttime", 0)->plaintext);
			$boxDate = $movie->find(".box_date", 0);
			$day = $boxDate->find(".day", 0)->plaintext;
			$month = $boxDate->find("span", 1)->plaintext;
			if (trim("$day $month") !== $dayFormatted) {
				continue;
			}
			$link = html_entity_decode($movie->find("h2 a", 0)->href);

			$movies[] = $this->getMovieDetail($link, clone $when);;
		}

		return $movies;
	}

	protected function trim(&$val, $key)
	{
		if (!empty($val)) {
			$val = trim($val, "\n\r\t ");
		} else {
			$val = NULL;
		}

	}

	function cesky_mesic($mesic)
	{
		$mesic = (int)$mesic;
		static $nazvy = array(1 => 'led', 'úno', 'bře', 'dub', 'kvě', 'čvn', 'čvc', 'srp', 'zář', 'říj', 'lis', 'pro');

		return $nazvy[$mesic];
	}
}