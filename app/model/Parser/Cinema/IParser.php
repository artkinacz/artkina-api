<?php
namespace Parser\Cinema;

use DateTime;
use Parser\Entity\Movie;

/**
 * Interface for program providers.
 *
 * @author Jakub Fiser <jakubfiser@jakubfiser.com>
 */
interface IParser
{
	/**
	 * Fetches array of movies played on $day
	 * @param DateTime $day
	 * @return Movie[]
	 */
	public function getDay(DateTime $day);
}

