<?php

namespace Parser\Cinema;


use App\Parser\Generic\AerofilmsAPI;

class KinoScalaParser extends AerofilmsAPI
{

	const OVERALL_URL = "http://www.kinoscala.cz/1.0/export/showtimes";
	const DETAIL_URL = "http://www.kinoscala.cz/1.0/export/description/%id%";

	public function __construct()
	{
		parent::__construct(self::OVERALL_URL, self::DETAIL_URL);
	}


}