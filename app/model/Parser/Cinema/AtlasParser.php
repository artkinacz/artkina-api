<?php
namespace Parser\Cinema;

use App\Parser\Generic\CinemawareCrawler;

class AtlasParser extends CinemawareCrawler
{

	const PROGRAM_URL = "http://www.kinoatlas.cz/";
	const BASE_URL = "http://www.kinoatlas.cz/";

	function __construct()
	{
		parent::__construct(self::BASE_URL, self::PROGRAM_URL);
	}


}