<?php


namespace Parser\Cinema;


use Parser\Entity\Movie;

interface IPeriodAwareParser
{
	/**
	 * @param \DateTime $since
	 * @param $period
	 * @return Movie[]
	 */
	function getProgram(\DateTime $since, $period);

} 