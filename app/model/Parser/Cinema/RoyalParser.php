<?php
namespace Parser\Cinema;

use Nette\Utils\DateTime;
use Parser\Entity\Movie;
use Parser\Entity\Projection;
use simple_html_dom_node;
use Sunra\PhpSimple\HtmlDomParser;

class RoyalParser implements IParser
{

	const PROGRAM_URL = "http://www.leroyal.cz/program/?lang=cs";
	const BASE_URL = "http://www.leroyal.cz/";

	/**
	 * Fetches array of movies played on $day
	 * @param DateTime $day
	 * @return array
	 */
	public function getDay(\DateTime $day)
	{
		return $this->getDayProgram($day);
	}

	protected function getMovieDetail(simple_html_dom_node $movieDom, $imageUrl, DateTime $day)
	{

//        parse movie detail
		$movie = new Movie();
		$projection = new Projection();

//        movie title
		$title = $movieDom->find("strong", 0)->plaintext;
		$title = explode("-", $title);
		$title = trim(preg_replace("#^KINO#", "", trim($title[1])));
		$movie->setTitle($title);

//        data are from wysiwyg, so we iterate and search for the right p tag
		foreach ($movieDom->find("p") as $p) {
			$plain = $p->plaintext;
//            dump($plain);
//              time
			preg_match("#^Zač&aacute;tek: ([0-9]+)[.:]([0-9]+)#", trim($plain), $projectionTime);
			if (!empty($projectionTime)) {
				$day->setTime(trim($projectionTime[1]), trim($projectionTime[2]));
				$projection->setDate($day);
			}

//              projection price
			preg_match("#^Vstupn&eacute; ?: ([0-9]+)Kč#", trim($plain), $projectionPrice);
			if (!empty($projectionPrice)) {
				$projection->setPrice($projectionPrice[1]);
			}

//              projection language (we search for it via fulltext :)
			foreach (array("titulky", "Česky", "česky") as $pattern) {
				if (preg_match("#$pattern#", trim($plain))) {
					$projection->setLanguage(html_entity_decode(trim(preg_replace("#<!--.*-->#", "", $p->innertext))));
				}
			}

//              year
			preg_match("#([0-9]{4})#", trim($plain), $yearMatch);
//            dump($yearMatch);
			if (!empty($yearMatch)) {
				$movie->setYear($yearMatch[0]);
			}
		}


//        we are looking for description from the end of the array (skipping empty p tags)
		$pTags = $movieDom->find("p");
		for ($x = count($pTags) - 1; $x >= 0; $x--) {
			$p = $movieDom->find("p", $x)->plaintext;
			$p = trim($p, "\n\r\t ");
			if (empty($p) || $p == "&nbsp;") {
				continue;
			}
			$movie->setDescription(html_entity_decode($p));
			break;
		}

//        image
		$movie->setImage($imageUrl);

//        projection
		$movie->setProjection($projection);

//        dump($movie);

		return $movie;
	}

	/**
	 * @param DateTime $day
	 * @return array
	 */
	protected function getDayProgram(DateTime $day)
	{
		$when = clone $day;
		$html = HtmlDomParser::file_get_html(self::PROGRAM_URL);
		$movies = [];
		setlocale(LC_TIME, "czech");
		foreach ($html->find(".content .row") as $movieRow) {
			$movieData = $movieRow->find(".col-md-6", 0);

			if (!$movieData) {
//                not an actual movie, just some html junk > skipping
				continue;
			}

//            check if wanted date match
			$day = $movieData->find('strong>span', 0)->plaintext;
			preg_match("#([0-9]+)\.([0-9]+)#", $day, $matches);
			if ($matches[1] != $when->format("d") || $matches[2] != $when->format("m")) {
//                not the day we are looking for > skipping
				continue;
			}

//            we now know we want to parse/persist this
			$movieImageSrc = static::BASE_URL . $movieRow->find(".col-md-6", 1)->find(".gallery img", 0)->src;
			$this->getMovieDetail($movieData, $movieImageSrc, $when);
			$movies[] = $this->getMovieDetail($movieData, $movieImageSrc, $when);
		}

		return $movies;
	}

	protected function trim(&$val, $key)
	{
		if (!empty($val)) {
			$val = trim($val, "\n\r\t ");
		} else {
			$val = NULL;
		}

	}
}