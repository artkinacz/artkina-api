<?php
namespace Parser\Cinema;

use Nette\Utils\DateTime;
use Parser\Entity\Movie;
use Parser\Entity\Projection;
use Sunra\PhpSimple\HtmlDomParser;

class MiniKinoParser implements IParser
{

	const PROGRAM_URL = "http://www.minikino.cz/default.aspx?iid=94";
	const BASE_URL = "http://www.minikino.cz/";

	/**
	 * Fetches array of movies played on $day
	 * @param DateTime $day
	 * @return array
	 */
	public function getDay(\DateTime $day)
	{
		return $this->getDayProgram($day);
	}


	protected function getMovieDetail($url, DateTime $day)
	{

		$html = HtmlDomParser::file_get_html($url);
		$mainContent = $html->find("#obsah table[width=725]", 0);
		$pravySloupec = $html->find("#pravy", 0);
		$levySloupec = $mainContent->find("td[width=210]", 0);

//        parse movie detail
		$movie = new Movie();
		$projection = new Projection();
		$movie->setTitle($mainContent->find("h1", 0)->plaintext);
		$description = $mainContent->find("div[style=font-size: 11px]", 0);
		if (!$description) {
			$description = $mainContent->find("div[style=font-size: 13px]", 0);
		}
		$movie->setDescription($description ? $description->plaintext : NULL);

//        get time of a projection
		foreach ($pravySloupec->find("a") as $projectionRow) {
			$datetime = DateTime::createFromFormat("d.m.Y H:i", $projectionRow->plaintext);
			if ($datetime->format("Y-m-d") == $day->format("Y-m-d")) {
				$projection->setDate(DateTime::from($datetime->getTimestamp()));
				break;
			}
		}
		$movie->setImage($mainContent->find("img", 0)->src);

		$levySloupecContent = $levySloupec->find("h4", 0)->innertext;
		$levySloupecContent = explode("<br />", $levySloupecContent);
		preg_match("/(\d+)/", $levySloupecContent[2], $year);
		$movie->setYear(array_shift($year));

		$movie->setDirector(substr($levySloupecContent[0], 7));
		$projection->setLanguage($levySloupecContent[3]);

		preg_match("/(\d+)/", $levySloupecContent[5], $length);
		$movie->setLength(array_shift($length));

		$priceText = $levySloupec->find("h4", 1)->innertext;
		preg_match("/([0-9.]+)/", $priceText, $price);
		$movie->setYear(array_shift($year));
		$projection->setPrice(array_shift($price));
		$movie->setProjection($projection);

		return $movie;
	}

	/**
	 * @param DateTime $day
	 * @return array
	 */
	protected function getDayProgram(DateTime $day)
	{
		$html = HtmlDomParser::file_get_html(self::PROGRAM_URL);
		$movies = [];
		$dayFormatted = $day->format("j.n.Y");
		$mainNode = $html->find("#obsah table[width=980]");
		if (empty($mainNode)) {
			throw new ParserException("Unable to find root node in " . get_class($this));
		}
		foreach ($mainNode as $movie) {
			$projectionStart = trim($movie->find("h3", 0)->plaintext);
			$projectionStart = trim(substr($projectionStart, 0, -5));

			if ($projectionStart !== $dayFormatted) {
				continue;
			}
			$link = self::BASE_URL . html_entity_decode($movie->find("h2 a", 0)->href);
			$movies[] = $this->getMovieDetail($link, $day);
		}

		return $movies;
	}

	protected function trim(&$val, $key)
	{
		if (!empty($val)) {
			$val = trim($val, "\n\r\t ");
		} else {
			$val = NULL;
		}

	}
}