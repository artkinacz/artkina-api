<?php


namespace Parser\Entity;


use Nette\Object;

class Movie extends Object
{

	private $title;

	private $titleOrig;

	private $year;

	private $description;

	private $image;

	private $projection;

	private $director;

	private $length;

	private $country;

	/**
	 * @return mixed
	 */
	public function getCountry()
	{
		return $this->country;
	}


	/**
	 * @return mixed
	 */
	public function getDescription()
	{
		return $this->description;
	}

	/**
	 * @return mixed
	 */
	public function getDirector()
	{
		return $this->director;
	}

	/**
	 * @return mixed
	 */
	public function getImage()
	{
		return $this->image;
	}

	/**
	 * @return mixed
	 */
	public function getLength()
	{
		return $this->length;
	}

	/**
	 * @param mixed $length
	 */
	public function setLength($length)
	{
		$this->length = $length;
	}

	/**
	 * @return Projection
	 */
	public function getProjection()
	{
		return $this->projection;
	}

	/**
	 * @param mixed $projection
	 */
	public function setProjection($projection)
	{
		$this->projection = $projection;
	}

	/**
	 * @return mixed
	 */
	public function getTitle()
	{
		return $this->title;
	}

	/**
	 * @return mixed
	 */
	public function getTitleOrig()
	{
		return $this->titleOrig;
	}


	/**
	 * @return mixed
	 */
	public function getYear()
	{
		return $this->year;
	}


	/**
	 * @param string $description
	 * @return $this
	 */
	public function setDescription($description)
	{
		$this->description = self::trim($description);

		return $this;
	}

	/**
	 * @param string $image
	 * @return $this
	 */
	public function setImage($image)
	{
		$this->image = self::trim($image) ?: NULL;

		return $this;
	}

	/**
	 * @param string $title
	 * @return $this
	 */
	public function setTitle($title)
	{
		$this->title = self::trim(self::stripCDDATA($title));

		return $this;
	}

	/**
	 * @param string $title_orig
	 * @return $this
	 */
	public function setTitleOrig($title_orig)
	{
		$this->titleOrig = self::trim(self::stripCDDATA($title_orig));

		return $this;
	}

	/**
	 * @param int $year
	 * @return $this
	 */
	public function setYear($year)
	{
		$this->year = (int)$year;

		return $this;
	}


	/**
	 * @param string $director
	 * @return $this
	 */
	public function setDirector($director)
	{
		$this->director = self::trim($director);

		return $this;
	}

	/**
	 * @param string $country
	 * @return $this
	 */
	public function setCountry($country)
	{
		$this->country = self::trim($country);

		return $this;
	}

	public static function stripCDDATA($string)
	{
		return preg_replace('/^\s*\/\/<!\[CDATA\[([\s\S]*)\/\/\]\]>\s*\z/', '$1', $string);

	}

	public static function trim($val)
	{
		return trim($val, "\n\r\t ");
	}

	public function toEntity()
	{
		$entity = new \App\Doctrine\Entity\Movie();

		foreach (get_object_vars($this) as $property => $value) {
			if ($property === 'projection') {
				continue;
			}
			$entity->$property = $value;
		}

		return $entity;
	}

} 