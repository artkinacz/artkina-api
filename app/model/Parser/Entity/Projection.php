<?php


namespace Parser\Entity;


use Nette\Object;

class Projection extends Object
{

	private $movie;
	private $date;
	private $price;
	private $language;

	/**
	 * @return mixed
	 */
	public function getDate()
	{
		return $this->date;
	}

	/**
	 * @return mixed
	 */
	public function getLanguage()
	{
		return $this->language;
	}

	/**
	 * @return mixed
	 */
	public function getMovie()
	{
		return $this->movie;
	}

	/**
	 * @return mixed
	 */
	public function getPrice()
	{
		return $this->price;
	}

	/**
	 * @param mixed $date
	 */
	public function setDate($date)
	{
		$this->date = $date;
	}

	/**
	 * @param mixed $language
	 */
	public function setLanguage($language)
	{
		$this->language = trim($language);
	}

	/**
	 * @param mixed $movie
	 */
	public function setMovie($movie)
	{
		$this->movie = $movie;
	}

	/**
	 * @param mixed $price
	 */
	public function setPrice($price)
	{
		$this->price = (int)$price;
	}


	public function toEntity()
	{
		$entity = new \App\Doctrine\Entity\Projection();

		foreach (get_object_vars($this) as $property => $value) {
			$entity->$property = $value;
		}

		return $entity;
	}


}