<?php
namespace Parser;

final class TmdbParser
{

	const API_KEY = 'fb3bdfb42a0312c1cfbdd94926deec00';
	const API_URL = 'https://api.themoviedb.org/3';
	const SLEEP_INTERVAL = 1;

	public function findMovies($title, $year)
	{
		sleep(self::SLEEP_INTERVAL);

		$url = self::API_URL . '/search/movie?' . http_build_query(array(
				'api_key' => self::API_KEY,
				'query'   => $title,
				'year'    => $year
			));
		$movies = json_decode(file_get_contents($url));

		return $movies;
	}

	public function getMovie($id)
	{
		sleep(self::SLEEP_INTERVAL);

		$url = self::API_URL . "/movie/{$id}?" . http_build_query(array(
				'api_key' => self::API_KEY
			));
		$movie = json_decode(file_get_contents($url));

		return $movie;
	}

	public function getTrailers($movieId)
	{
		sleep(self::SLEEP_INTERVAL);

		$url = self::API_URL . "/movie/{$movieId}/trailers?" . http_build_query(array(
				'api_key' => self::API_KEY
			));
		$trailers = json_decode(file_get_contents($url));

		return $trailers;
	}

}