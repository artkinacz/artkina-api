<?php


namespace App\Parser\Generic;


use Kdyby\Curl\CurlWrapper;
use Nette\Utils\DateTime;
use Nette\Utils\Json;
use Parser\Cinema\IParser;
use Parser\Cinema\ParserException;
use Parser\Entity\Movie;
use Parser\Entity\Projection;

abstract class AerofilmsAPI implements IParser
{
	protected $data = FALSE;

	private $overallUrl;

	private $detailUrl;

	function __construct($overallUrl, $detailUrl)
	{
		$this->detailUrl = $detailUrl;
		$this->overallUrl = $overallUrl;
	}


	/** {@inheritdoc} */
	public function getDay(\DateTime $day)
	{
		$key = $day->format("Y-m-d");

		if ($this->data === FALSE) {
			$this->loadData();
		}

		if (isset($this->data[$key])) {
			return $this->data[$key];
		}

		return [];
	}

	protected function loadData()
	{
		$weekly = $this->getOverallData();

		if (emptY($weekly)) {
			throw new ParserException("No weekly data found.");
		}

		$movies = [];
		foreach ($weekly as $item) {
			$movie = new Movie();
			$projection = new Projection();
			$projection->setDate(new DateTime($item->date));
			$projection->setMovie($movie);
			$projection->setPrice($item->price);
			$movie->setProjection($projection);
			$movies[] = $this->parseMovieDetail($movie, $projection, $item);
		}

		$this->data = [];

		/** @var Movie $movie */
		foreach ($movies as $movie) {
			if ($movie->getProjection() && $movie->getProjection()->date) {
				$this->data[$movie->getProjection()->date->format("Y-m-d")][] = $movie;
			}
		}
	}

	protected function getOverallData()
	{
		return $this->downloadJson($this->overallUrl);
	}

	protected function downloadJson($url)
	{
		$curl = new CurlWrapper($url);

		$curl->execute();

		$response = $curl->response;

		if ($response) {
			return Json::decode($response)->film;
		}

		return [];
	}

	protected function parseMovieDetail(Movie $movie, Projection $projection, $item)
	{
		$detail = $this->downloadJson(str_replace("%id%", $item->id, $this->detailUrl));

		$movie->setTitle(trim($detail->name));
		$movie->setTitleOrig(trim($detail->original));
		if (isset($detail->year)) {
			$movie->setYear($detail->year ?: NULL);
		}
		if (isset($detail->runtime)) {
			$movie->setLength($detail->runtime ?: NULL);
		}
		if (isset($detail->countries)) {
			$movie->setCountry($detail->countries);
		}

		if (isset($detail->plot)) {
			$movie->setDescription(trim($detail->plot));
		}
		if (isset($detail->image)) {
			$movie->setImage($detail->image);
		}

		if (isset($detail->languages)) {
			$projection->setLanguage($detail->languages);
		}

		return $movie;
	}
}