<?php


namespace App\Parser\Generic;


use Nette\Utils\DateTime;
use Parser\Cinema\IParser;
use Parser\Cinema\ParserException;
use Parser\Entity\Movie;
use Parser\Entity\Projection;
use Sunra\PhpSimple\HtmlDomParser;

abstract class CinemawareCrawler implements IParser
{

	private $baseUrl;
	private $programUrl;

	public function __construct($baseUrl, $programUrl)
	{
		$this->baseUrl = $baseUrl;
		$this->programUrl = $programUrl;
	}


	/**
	 * Fetches array of movies played on $day
	 * @param DateTime $day
	 * @return array
	 */
	public function getDay(\DateTime $day)
	{
		return $this->getDayProgram($day);
	}

	protected function getMovieDetail($url, DateTime $day, $price, $length, $titleOrig = FALSE)
	{

		$html = HtmlDomParser::file_get_html($this->baseUrl . $url);
		$mainContent = $html->find("#lavyPanel", 0);
//        parse movie detail
		$movie = new Movie();
		$projection = new Projection();

//        movie title
		$movie->setTitle(trim($mainContent->find("h1", 0)->plaintext));
		if ($titleOrig) {
			$movie->setTitleOrig(trim($titleOrig));
		}

		$imageUrl = $mainContent->find(".hlavnePlatnoPlatno", 0)->find("img", -1);

		if ($imageUrl) {
			$movie->setImage($imageUrl->src);
		}

//        top right box
		$topRightBox = $mainContent->find(".right table", 0);

		$director = $topRightBox->find("td", 1);
		if ($director) {
			$movie->setDirector(trim($director->plaintext));
		}

		$year = $topRightBox->find("td", 5);
		if ($year) {
			$year = substr($year->plaintext, -4);
			if (is_numeric($year) && $year > 1900) {
				$movie->setYear($year);
			} else {
				$year = $topRightBox->find("td", 3);
				if ($year) {
					$year = substr($year->plaintext, -4);
					$movie->setYear($year);
				}
			}

		}

//        length
		$length = str_replace("&nbsp;", " ", $length);
		$movie->setLength(trim($length));


//        time
		$projection->setDate(DateTime::from($day->getTimestamp()));

//        price
		$projection->setPrice(trim($price));

		//        description
		$infoBox = $mainContent->find(".bigpad", 0);
		$desc = $infoBox->find("p", 1);
		if ($desc) {
			$movie->setDescription(trim($desc->plaintext));
		}

//        language
		$language = $mainContent->find(".right .object span", 1);
		if ($language) {
			$projection->setLanguage($language->title);
		}


		array_walk($movie, array($this, "trim"));
		array_walk($projection, array($this, "trim"));

		$movie->setProjection($projection);

		return $movie;
	}

	/**
	 * @param DateTime $day
	 * @return array
	 */
	protected function getDayProgram(DateTime $day)
	{
		$today = new DateTime();
		$today->setTime(0, 0, 0);
		$when = clone $day;
		$when->setTime(0, 0, 0);

		$dayNumber = $today->diff($when)->days + 1;
		if ($dayNumber > 10) {
			throw new \BadMethodCallException("Evald program is just 10 days to the future");
		}

		$html = HtmlDomParser::file_get_html($this->programUrl);
		$movies = [];
		setlocale(LC_TIME, "czech");

		$mainContent = $html->find("div[id=den_$dayNumber] table.program2 tr");
		if (empty($mainContent)) {
			throw new ParserException("Unable to find root node in " . get_class($this));
		}
		foreach ($mainContent as $movie) {
			$link = html_entity_decode($movie->find(".name a", 0)->href);
			$length = trim($movie->find(".lenght", 0)->find("span", 0)->plaintext);
			$price = trim($movie->find(".lenght", 1)->plaintext);
			$titleOrig = $movie->find(".name small", 0);
			$titleOrig = $titleOrig ? trim($titleOrig->plaintext, " ()\n") : FALSE;
			foreach ($movie->find(".time a, .time span") as $timeText) {
				$timeArray = explode(":", trim($timeText->plaintext));
				$projectionTime = clone $when;
				$projectionTime->setTime((int)$timeArray[0], (int)$timeArray[1], 0);
				$movies[] = $this->getMovieDetail($link, $projectionTime, $price, $length, $titleOrig);
			}

		}

		return $movies;
	}

	protected function trim(&$val, $key)
	{
		if (is_string($val)) {
			if (!empty($val)) {
				$val = str_replace("&nbsp;", " ", $val);
				$val = trim($val, "\n\r\t- ");
			} else {
				$val = NULL;
			}
		}
	}
}