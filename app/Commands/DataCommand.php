<?php


namespace App\Commands;


use App\Doctrine\Entity\Cinema;
use App\Doctrine\Entity\Movie;
use App\Doctrine\Facade\CinemaFacade;
use App\Doctrine\Facade\MovieFacade;
use App\Doctrine\Facade\ProjectionFacade;
use App\Logger\Logger;
use Consumerr\Consumerr;
use Kdyby\Doctrine\EntityManager;
use Nette;
use Parser\Cinema\IParser;
use Parser\Cinema\IPeriodAwareParser;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class DataCommand extends Command
{
	private static $cinemas = [

		"svetozor" => '\Parser\Cinema\SvetozorRegexpParser',
		"aero" => '\Parser\Cinema\AeroRegexpParser',
		"biooko" => '\Parser\Cinema\BioOkoRegexpParser',
		"kinoskala" => '\Parser\Cinema\KinoScalaParser',
		"minikino" => '\Parser\Cinema\MiniKinoParser',
		"lucerna_brno" => '\Parser\Cinema\LucernaBrnoParser',
		"kino-art" => '\Parser\Cinema\ArtKinoParser',
		"evald" => '\Parser\Cinema\EvaldParser',
		"atlas" => '\Parser\Cinema\AtlasParser',
		"mat" => '\Parser\Cinema\KinoMatParser',
		'biocentral' => '\Parser\Cinema\BiocentralParser',
		'lucernapraha' => '\Parser\Cinema\KinoLucernaParser',
		//"royal" => '\Parser\Cinema\RoyalParser',
	];
	/** @var Logger */
	private $logger;
	/** @var CinemaFacade */
	private $cinemaFacade;

	const DAYS_TO_FETCH = 7;
	/** @var ProjectionFacade */
	private $projectionFacade;
	/** @var MovieFacade */
	private $movieFacade;
	/** @var EntityManager */
	private $em;

	function __construct(CinemaFacade $cinemaFacade, EntityManager $em, Logger $logger, MovieFacade $movieFacade, ProjectionFacade $projectionFacade)
	{
		parent::__construct();
		$this->cinemaFacade = $cinemaFacade;
		$this->em = $em;
		$this->logger = $logger;
		$this->movieFacade = $movieFacade;
		$this->projectionFacade = $projectionFacade;
	}


	protected function configure()
	{
		$this->setName('app:update');
	}


	protected function execute(InputInterface $input, OutputInterface $output)
	{

		$this->logger->log("Spuštěna aktualizace programů kin", Logger::TYPE_CRON_INFO);
		$cinemas = $this->cinemaFacade->findAssoc(array(), "slug");

		//execute each parser
		foreach (self::$cinemas as $cinemaKey => $className) {
			try {
				$cinema = $cinemas[$cinemaKey];
				$data = $this->executeParser($className, $cinemas[$cinemaKey]);

				//save
				foreach ($data as $day => $movies) {

					$this->projectionFacade->disableDaysProjections($day, $cinema);
					/** @var \Parser\Entity\Movie $movie */
					foreach ($movies as $movie) {
						if (!$movie || !($projection = $movie->getProjection())) {
							continue;
						}
						$movieEntity = $this->processMovie($movie);

						$projectionEntity = $this->projectionFacade->findOneBy(array("date" => $projection->getDate(), "cinema" => $cinema));
						if (empty($projectionEntity)) {
							$projectionEntity = $projection->toEntity();
						}
						$projectionEntity
							->setEnabled(TRUE)
							->setCinema($cinema)
							->setMovie($movieEntity);
						$this->em->persist($projectionEntity);

					}
				}

				$this->em->flush();
			} catch (\Exception $e) {
				$this->logger->log($e->getFile() . ":" . $e->getLine() . " - " . $e->getMessage(),
					Logger::TYPE_CRON_ERROR);
				Nette\Diagnostics\Debugger::log($e);
			}

		}

		$message = "Stahování a ukládání nových programů dokončeno";
		echo $message . "\n";
		$this->logger->log($message, Logger::TYPE_CRON_INFO);
	}

	/**
	 * @param $className
	 * @param Cinema $cinema
	 * @return array|\Parser\Entity\Movie[]
	 */
	private function executeParser($className, Cinema $cinema)
	{
		$data = [];
		$counter = 0;
		/** @var IParser|IPeriodAwareParser $handler */
		$handler = new $className();
		$day = new Nette\Utils\DateTime();
		if ($handler instanceof IPeriodAwareParser) {
			$data = $handler->getProgram($day, static::DAYS_TO_FETCH);
			ConsumErr::addEvent("data", $cinema->slug, count($data) . ' days');
		} else {
			for ($x = 0; $x < static::DAYS_TO_FETCH; $x++) {
				$dayData = (array)$handler->getDay($day);
				$counter += count($dayData);
				$data[$day->format("Y-m-d")] = array_filter($dayData);
				$day->modify("+ 1 day");
			}
			ConsumErr::addEvent("data", $cinema->slug, $counter . ' projections');
		}

		$data = array_filter($data);
		$message = "Parsed {$cinema->slug} - found movies for " . count($data) . ' days, ' . $counter . ' projections';
		echo $message . "\n";
		$this->logger->log($message, Logger::TYPE_CRON_INFO);

		return $data;
	}

	/**
	 * @param \Parser\Entity\Movie $movie
	 * @return Movie
	 */
	private function processMovie(\Parser\Entity\Movie $movie)
	{
		$entity = $this->movieFacade->findOneBy(array("title" => $movie->getTitle()));
		if (!$entity) {

			if ($movie->getImage()) {
				$movie->setImage($this->movieFacade->saveImage($movie->getImage()));
			}
			$entity = $movie->toEntity();
			$this->em->persist($entity);

		}

		return $entity;
	}


} 