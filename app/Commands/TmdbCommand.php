<?php


namespace App\Commands;


use App\Doctrine\Entity\Tmdb;
use App\Doctrine\Entity\Movie;
use App\Doctrine\Facade\TmdbFacade;
use App\Doctrine\Facade\MovieFacade;
use App\Logger\Logger;
use Kdyby\Doctrine\EntityManager;
use Nette\Diagnostics\Debugger;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class TmdbCommand extends Command
{

	const LIMIT = 1;

	/** @var /Parser/TmdbParser */
	private $tmdbParser;

	/** @var /Parser/ImdbParser */
	private $imdbParser;

	/** @var MovieFacade */
	private $movieFacade;

	/** @var TmdbFacade */
	private $tmdbFacade;

	/** @var Logger */
	private $logger;

	/**
	 * @var EntityManager
	 */
	private $em;

	public function __construct(Logger $logger, MovieFacade $movieFacade, TmdbFacade $tmdbFacade, EntityManager $em)
	{
		parent::__construct();

		$this->logger      = $logger;
		$this->movieFacade = $movieFacade;
		$this->tmdbFacade  = $tmdbFacade;
		$this->tmdbParser  = new \Parser\TmdbParser;
		$this->imdbParser  = new \Parser\ImdbParser;
		$this->em          = $em;
	}


	protected function configure()
	{
		$this->setName('app:tmdb')
			->addOption('limit', 'l', InputOption::VALUE_REQUIRED, NULL, self::LIMIT);
	}


	protected function execute(InputInterface $input, OutputInterface $output)
	{
		$this->logger->log("Spuštěna aktualizace TMDB dat", Logger::TYPE_CRON_INFO);
		$movies = $this->movieFacade->getMoviesToTmdbUpdate($input->getOption('limit'));
		foreach ($movies as $movie) {
			$status = $this->getTmdbData($movie, $movie->getYear(), $movie->getTitleOrig());
			if (!$status) {
				$this->getTmdbData($movie, $movie->getYear(), $movie->getTitle());
			}
		}
		$this->logger->log("Aktualizace TMDB dat ukončena", Logger::TYPE_CRON_INFO);
	}


	private function getTmdbData($movie, $year, $title)
	{

		$id = $movie->getId();

		try {
			$this->logger->log("Hledám TMDB data pro film ID #{$id} / {$title} ({$year})", Logger::TYPE_CRON_INFO);
			$tmdbMovies = $this->tmdbParser->findMovies($title, $year);
		} catch (\Exception $e) {
			$this->logger->log($e->getFile() . ":" . $e->getLine() . " - " . $e->getMessage(), Logger::TYPE_CRON_ERROR);
			Debugger::log($e);
			return FALSE;
		}

		if (!isset($tmdbMovies->results) || !is_array($tmdbMovies->results)) {
			$movie->setTmdbStatus(FALSE);
			$this->em->flush();
			$this->logger->log("Data pro film ID #{$id} / {$title} ({$year}) nenalezena.", Logger::TYPE_CRON_INFO);
			return FALSE;
		}

		$tmdbStatus = FALSE;
		foreach ($tmdbMovies->results as $tmdbMovie) {

			if (strcasecmp($tmdbMovie->original_title, $title) !== 0) {
				continue;
			}

			$tmdbMovieDetail = $this->tmdbParser->getMovie($tmdbMovie->id);
			$imdbData = $this->imdbParser->getMovie($tmdbMovieDetail->imdb_id);

			$tmdbMovie = (object) array_merge((array) $tmdbMovie, (array) $tmdbMovieDetail);

			$tmdbEntity = $movie->tmdb;
			if ($tmdbEntity === NULL) {
				$tmdbEntity = new Tmdb();
			}

			$tmdbEntity->setMovie($movie);
			$tmdbEntity = $this->tmdbFacade->fillEntityByMovieData($tmdbEntity, $tmdbMovie, $imdbData);

			$trailers = $this->tmdbParser->getTrailers($tmdbMovie->id);

			if (isset($trailers->youtube) && is_array($trailers->youtube)) {
				$youtubeId = NULL;
				foreach ($trailers->youtube as $youtube) {
					if (strtolower($youtube->type) == "trailer") {
						$youtubeId = $youtube->source;
					}
					if (strtolower($youtube->type) == "trailer" && strtolower($youtube->size) == "hd") {
						$youtubeId = $youtube->source;
					}
				}
				$tmdbEntity->setYoutube($youtubeId);
				$this->logger->log("Trailer nalezen pro film ID #{$id} / {$title} ({$year})", Logger::TYPE_CRON_INFO);
			}

			$this->em->persist($tmdbEntity);
			$movie->setTmdbUpdate(new \DateTime());
			$tmdbStatus = TRUE;
			$this->logger->log("Data aktualizována pro film ID #{$id} / {$title} ({$year})", Logger::TYPE_CRON_INFO);
		}

		$movie->setTmdbStatus($tmdbStatus);
		$this->em->flush();

		if (!$tmdbStatus) {
			$this->logger->log("Data pro film ID #{$id} / {$title} ({$year}) nenalezena.", Logger::TYPE_CRON_INFO);
		}

		return $tmdbStatus;

	}

}