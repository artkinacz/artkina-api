<?php


namespace App\Commands;


use App\Doctrine\Facade\NotificationFacade;
use Nette\Diagnostics\Debugger;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class NotificationCommand extends Command
{
	/** @var NotificationFacade */
	private $notificationFacade;

	public function __construct(NotificationFacade $notificationFacade)
	{
		parent::__construct();
		$this->notificationFacade = $notificationFacade;
	}

	protected function configure()
	{
		$this->setName('app:notifications')
			->setDescription('Sends notifications.')
			->addOption('id', NULL, InputOption::VALUE_REQUIRED, 'send notification by specific if');
	}


	protected function execute(InputInterface $input, OutputInterface $output)
	{
		$qb = $this->notificationFacade->createQueryBuilder("n")->select("n");

		if ($input->getOption('id')) {
			$qb->where("n.id = :id")->setParameter("id", $input->getOption('id'));
		} else {
			$qb->where("n.timestamp <= CURRENT_TIMESTAMP() AND n.sent IS NULL");
		}

		$notifications = $qb->getQuery()->getResult();
		foreach ($notifications as $notification) {
			try {
				$this->notificationFacade->send($notification);
			} catch (\Exception $e) {
				Debugger::log($e);
			}
		}
	}


} 