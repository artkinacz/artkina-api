<?php


namespace App\Commands;


use App\Doctrine\Entity\Csfd;
use App\Doctrine\Entity\Movie;
use App\Doctrine\Facade\CsfdFacade;
use App\Doctrine\Facade\MovieFacade;
use App\Logger\Logger;
use Kdyby\Doctrine\EntityManager;
use Nette\Diagnostics\Debugger;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class CsfdCommand extends Command
{
	private $csfdParser;
	/** @var MovieFacade */
	private $movieFacade;
	/** @var Logger */
	private $logger;
	/** @var CsfdFacade */
	private $csfdFacade;
	/**
	 * @var EntityManager
	 */
	private $em;

	public function __construct(CsfdFacade $csfdFacade, Logger $logger, MovieFacade $movieFacade, EntityManager $em)
	{
		$this->csfdParser = new \Parser\Csfd();
		parent::__construct();
		$this->csfdFacade = $csfdFacade;
		$this->logger = $logger;
		$this->movieFacade = $movieFacade;
		$this->em = $em;
	}


	protected function configure()
	{
		$this->setName('app:csfd')
			->addOption('limit', 'l', InputOption::VALUE_REQUIRED, NULL, 20);
	}


	protected function execute(InputInterface $input, OutputInterface $output)
	{

		$movies = $this->movieFacade->findBy(array('csfdStatus' => NULL), NULL, $input->getOption('limit'));

		foreach ($movies as $movie) {
			$this->pairCsfdMovie($movie);
		}
	}

	private function pairCsfdMovie(Movie $movie)
	{

		try {
			$csfdMovieOverViews = $this->csfdParser->findMovies($movie->getTitle());
		} catch (\Exception $e) {
			$this->logger->log($e->getFile() . ":" . $e->getLine() . " - " . $e->getMessage(), Logger::TYPE_CRON_ERROR);
			Debugger::log($e);

			return FALSE;
		}

		$csfdMovieDetail = FALSE;

		foreach ($csfdMovieOverViews as $csfdMovieOverView) {
			if (
				isset($csfdMovieOverView->names->cs) &&
				trim($movie->getTitle()) == trim(strip_tags($csfdMovieOverView->names->cs)) &&
				$csfdMovieOverView->year == $movie->year
			) {
				$csfdMovieDetail = $this->csfdParser->getMovie($csfdMovieOverView->id);
				break;
			}
		}

		if ($csfdMovieDetail !== FALSE) {
			if (!$csfdMovieDetail) {
				return;
			}
			$csfdEntity = new Csfd();
			$csfdEntity->setMovie($movie);
			$csfdEntity = $this->csfdFacade->fillEntityByCsfdData($csfdMovieDetail, $csfdEntity);

			$this->em->persist($csfdEntity);

			$movie->setCsfdStatus(TRUE);
			$this->em->persist($movie);

		} else {
			$movie->setCsfdStatus(FALSE);
		}
		$this->em->flush();

		return $csfdMovieDetail;

	}


} 