<?php


namespace App\Utils;


class Mixed
{

	public static function camelToUnderscore($array)
	{
		$result = [];
		foreach ($array as $key => $value) {
			$key = strtolower(preg_replace('/([a-z])([A-Z])/', '$1_$2', $key));
			$result[$key] = $value;
		}

		return $result;

	}

} 