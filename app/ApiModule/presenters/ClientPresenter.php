<?php


namespace App\ApiModule;


use App\Doctrine\Entity\Client;
use Kdyby\Doctrine\EntityDao;
use Nette\Http\Request;

class ClientPresenter extends BasePresenter
{

	/** @var Request @autowire */
	protected $httpRequest;
	/** @var EntityDao @autowire(\App\Doctrine\Entity\Client, factory=\Kdyby\Doctrine\EntityDaoFactory) */
	protected $dao;

	public function actionNewInstall()
	{
		if (!$this->httpRequest->isPost()) {
			$this->sendError(\StatusCodes::HTTP_METHOD_NOT_ALLOWED);
		}
		$uuid = $this->httpRequest->getPost('uuid');

		if (!$uuid) {
			$this->sendError(\StatusCodes::HTTP_BAD_REQUEST);
		}

		if (!$this->dao->find($uuid)) {
			$this->dao->save(new Client($uuid));
		}

		$this->setStatus(\StatusCodes::HTTP_NO_CONTENT);
	}

} 