<?php
namespace App\ApiModule;

use App\Doctrine\Facade\MovieFacade;
use App\Doctrine\Facade\ProjectionFacade;
use Model;
use Nette;


/**
 * Homepage presenter.
 */
class MoviePresenter extends BasePresenter
{
	/**
	 * @var MovieFacade
	 */
	public $movieFacade;

	/**
	 * @var ProjectionFacade
	 */
	public $projectionFacade;

	public function renderDefault($id)
	{

	}

	public function actionDetail($id)
	{
		$movie = $this->movieFacade->findOneBy(array("id" => $id));
		if ($movie != NULL) {
			$result = $movie->toArray();
			$result['csfd'] = $movie->csfd ? $movie->csfd->toArray() : [];
			$this->setData($result);
		} else {
			$this->sendError(\StatusCodes::HTTP_NOT_FOUND, "Movie was not found");
		}
	}

	/**
	 * @param MovieFacade $movieFacade
	 */
	public function injectMovieFacade(MovieFacade $movieFacade)
	{
		$this->movieFacade = $movieFacade;
	}

}
