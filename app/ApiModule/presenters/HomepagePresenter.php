<?php
namespace App\ApiModule;

use App\Doctrine\Facade\CinemaFacade;
use Model;
use Nette;


/**
 * Homepage presenter.
 */
class HomepagePresenter extends BasePresenter
{

	/**
	 * @var CinemaFacade @inject
	 */
	public $cinemaFacade;


	public function renderDefault()
	{
		$hash = $this->getParameter("hash", FALSE);
		if ($hash != "goodshape") {
			throw new Nette\Application\BadRequestException("Špatný kontrolní hash pro zobrazení domovské stránky Api");
		}
		$cinemas = $this->cinemaFacade->findAll();
		$this->template->cinemas = $cinemas;
	}

	public function handleExampleDownload($type)
	{
		if (!in_array($type, ['xml', 'json'])) {
			throw new Nette\Application\BadRequestException;
		}

		$this->sendResponse(new Nette\Application\Responses\FileResponse(__DIR__ . '/../templates/Homepage/example.' . $type));
	}

	public function afterRender()
	{
		//needed to override JSON send
	}


}
