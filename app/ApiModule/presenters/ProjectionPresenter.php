<?php
namespace App\ApiModule;

use App\Doctrine\Entity\Movie;
use App\Doctrine\Entity\Projection;
use App\Doctrine\Facade\CinemaFacade;
use App\Doctrine\Facade\MovieFacade;
use App\Doctrine\Facade\ProjectionFacade;
use Doctrine\ORM\Query;
use Model;
use Nette;


/**
 * Homepage presenter.
 */
class ProjectionPresenter extends BasePresenter
{
	/**
	 * @var MovieFacade
	 */
	public $movieFacade;

	/**
	 * @var ProjectionFacade
	 */
	public $projectionFacade;

	/**
	 * @var CinemaFacade
	 */
	public $cinemaFacade;

	public function renderList($slugs, $until, $since)
	{
		$since = new Nette\Utils\DateTime($since ? $since : "now");
		$until = new Nette\Utils\DateTime($until ? $until : "+ 7 days");
		$cinemas = $this->cinemaFacade->findBy(array("slug" => explode(",", urldecode($slugs)), "active" => 1));

		if (empty($cinemas)) {
			$this->sendError(404, 'No cinema found.');
		}

		$projections = $this->projectionFacade->getProjections($since, $until, $cinemas);

		$result = [];
		/** @var Projection $item */
		foreach ($projections as $item) {

			$projection = array_filter($item->toArray());
			/** @var Movie $movie */
			$movie = $item->movie;

			$csfdLink = isset($movie->csfd->csfdId) ? $movie->csfd->csfdId : NULL;
			$projection['csfd_link'] = $csfdLink ? "http://www.csfd.cz/film/$csfdLink" : NULL;

			$csfdRating = isset($movie->csfd->rating) ? $movie->csfd->rating : NULL;
			$projection['csfd_rating'] = $csfdRating;

			$movie->length = (empty($movie->length) && !empty($movie->csfd->length)) ? $movie->csfd->length : $movie->length;

			$projection['movie'] = $movie->toArray();
			$projection['display'] = array();

			if ($movie->tmdb && $movie->tmdb->youtube != '') {
				$projection['movie']['youtube'] = $movie->tmdb->youtube;
			}

			if (!empty($movie->director)) {
				$projection['display'][] = array("label" => "Režie", "value" => $movie->director);
			}

			if (!empty($projection['language'])) {
				$projection['display'][] = array("label" => "Znění", "value" => $projection['language']);
			}

			if (!empty($movie->csfd->year)) {
				$projection['display'][] = array("label" => "Rok", "value" => $movie->csfd->year);
			} elseif (!empty($movie->year)) {
				$projection['display'][] = array("label" => "Rok", "value" => $movie->year);
			}

			if (!empty($movie->length)) {
				$projection['display'][] = array("label" => "Délka", "value" => $movie->length);
			}

			if ($csfdRating > 0) {
				$projection['display'][] = array("label" => "ČSFD", "value" => $csfdRating . '%');
			}
			if ($item->cinema->phone) {
				$projection['display'][] = [
					'label' => 'Rezervace',
					'value' => $item->cinema->phone,
					'link' => $this->getPhoneLink($item->cinema->phone),
					'style' => 'bold',
				];
			}
			if ($movie->csfd && $movie->csfd->getLink()) {
				$projection['display'][] = [
					'label' => 'ČSFD link',
					'value' => $movie->csfd->getLink(),
					'link' => $movie->csfd->getLink(),
				];
			}

			$result[] = $projection;

		}

		$this->setData($result);
	}

	/**
	 * @param MovieFacade $movieFacade
	 */
	public function injectMovieFacade(MovieFacade $movieFacade)
	{
		$this->movieFacade = $movieFacade;
	}

	/**
	 * @param ProjectionFacade $projectionFacade
	 */
	public function injectProjectionFacade(ProjectionFacade $projectionFacade)
	{
		$this->projectionFacade = $projectionFacade;
	}

	/**
	 * @param CinemaFacade $cinemaFacade
	 */
	public function injectCinemaFacade(CinemaFacade $cinemaFacade)
	{
		$this->cinemaFacade = $cinemaFacade;
	}

	private function getPhoneLink($phone)
	{
		$phone = str_replace([' ', '-'], '', $phone);

		if(!Nette\Utils\Strings::startsWith($phone, '+')) {
			$phone = '+420'.$phone;
		}
		return 'tel://'.$phone;
	}

}
