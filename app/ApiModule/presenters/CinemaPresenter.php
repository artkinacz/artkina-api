<?php
namespace App\ApiModule;

use App\Doctrine\Facade\CinemaFacade;
use App\Doctrine\Facade\ProjectionFacade;
use Model;
use Nette;


/**
 * Homepage presenter.
 */
class CinemaPresenter extends BasePresenter
{
	/**
	 * @var CinemaFacade
	 */
	public $cinemaFacade;

	/**
	 * @var ProjectionFacade
	 */
	public $projectionFacade;

	public function renderDefault()
	{
		$cinemas = $this->cinemaFacade->findBy(array("active" => 1));
		$cinemasOutput = array();
		foreach ($cinemas as $cinema) {
			if ($cinema->city) {
				$cinemasOutput[$cinema->city][] = $cinema;
			} else {
				$cinemasOutput["nocity"][] = $cinema;
			}
		}
		$this->setData($cinemasOutput);
	}

	public function actionProjection($slugs, $until, $since)
	{
		$since = new Nette\Utils\DateTime($since ? $since : "now");
		$until = new Nette\Utils\DateTime($until ? $until : "+ 7 days");
		$cinemas = $this->cinemaFacade->findBy(array("slug" => explode(",", $slugs)));
		if ($cinemas != NULL) {
			$projection = array();
			foreach ($cinemas as $cinema) {
				$projection[$cinema->slug] = $this->projectionFacade->createQuery("SELECT p FROM \App\Doctrine\Entity\Projection p WHERE p.date BETWEEN :since AND :until AND p.cinema_id = :cinema_id")
					->setParameters(array("since" => $since, "until" => $until, "cinema_id" => $cinema->getId()))
					->getArrayResult();
			}
			$this->setData($projection);
		} else {
			$this->sendError(\StatusCodes::HTTP_NOT_FOUND, "Any cinema was not found");
		}
	}

	public function actionDetail($id)
	{
//        die($id);
		$cinema = $this->cinemaFacade->findOneBy(array("slug" => $id));
		if ($cinema != NULL) {
			$this->setData($cinema);
		} else {
			$this->sendError(\StatusCodes::HTTP_NOT_FOUND, "Cinema was not found");
		}
	}

	/**
	 * @param CinemaFacade $cinemaFacade
	 */
	public function injectCinemaFacade(CinemaFacade $cinemaFacade)
	{
		$this->cinemaFacade = $cinemaFacade;
	}

	/**
	 * @param ProjectionFacade $projectionFacade
	 */
	public function injectProjectionFacade(ProjectionFacade $projectionFacade)
	{
		$this->projectionFacade = $projectionFacade;
	}

}
