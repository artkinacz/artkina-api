<?php
namespace App\ApiModule;

use Nette;


/**
 * Base presenter for all application presenters.
 */
abstract class BasePresenter extends \App\BasePresenter
{
	public $data;
	public $status;

	public function startup()
	{
		parent::startup();
		$this->data = new Nette\Utils\ArrayHash();
		$now = new Nette\Utils\DateTime();
		$this->status = array(
			"code" => \StatusCodes::HTTP_OK,
			"message" => \StatusCodes::getMessageForCode(\StatusCodes::HTTP_OK),
			"time" => $now->format(Nette\Utils\DateTime::ISO8601)
		);
	}

	public function beforeRender()
	{

	}

	public function afterRender()
	{

		$response = new Nette\Utils\ArrayHash();
		$response->status = $this->status;
		$response->data = $this->getData();
//        dump($response);die();
//        Je žádoucí dostávat v aplikaci vždy pouze status code 200
		$this->getHttpResponse()->setCode(\StatusCodes::HTTP_OK);
		$this->sendJson($response);
		$this->terminate();
	}

	public function sendError($code, $message = FALSE)
	{
		$this->setStatus($code, $message);
//        Je žádoucí dostávat v aplikaci vždy pouze status code 200
		$this->getHttpResponse()->setCode(\StatusCodes::HTTP_OK);
		$this->sendJson(array("status" => $this->status));
		$this->terminate();
	}

	public function setStatus($code, $message = FALSE)
	{
		$this->status = array(
			"code" => $code,
			"message" => $message ? $message : \StatusCodes::getMessageForCode($code)
		);
	}

	public function getData()
	{
		return $this->data;
	}

	public function setData($data)
	{
		$this->data = $data;

		return $this;
	}

	public function addData($data)
	{
		$this->data = array_merge($this->data, $data);

		return $this;
	}
}
