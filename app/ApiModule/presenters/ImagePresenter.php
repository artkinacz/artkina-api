<?php
namespace App\ApiModule;

use Nette\Utils\Image;
use Nette\Utils\UnknownImageFileException;

/**
 * Image presenter.
 * @author Jaromír Navara <jaromir.navara@goodshape.cz>
 */
class ImagePresenter extends BasePresenter
{

	public function actionDefault($width, $height, $filename, $crop)
	{
		$params = $this->context->getParameters();
		$wwwDir = $params['wwwDir'];
		$imagesDir = $wwwDir . DIRECTORY_SEPARATOR . "images" . DIRECTORY_SEPARATOR . "movie";
		$resizedDir = $imagesDir . DIRECTORY_SEPARATOR . "resized";
		if (!is_dir($resizedDir)) {
			@mkdir($resizedDir, 0777, TRUE);
		}
		$tmpFilename = $crop == "crop" ? "c_$filename" : $filename;

		$wantedFileName = "{$width}x{$height}-{$tmpFilename}";

//        find the picture
		$filepath = $imagesDir . DIRECTORY_SEPARATOR . $filename;
		if (!file_exists($filepath)) {
			$this->sendError(\StatusCodes::HTTP_NOT_FOUND, "Image cannot be found");
		}

		try {
//        find resized picture (if exists) [filename could be 200x200-16.jpg]
			if (file_exists($resizedDir . DIRECTORY_SEPARATOR . $wantedFileName)) {
//            if it's not an invalid old cached image, return it
				if (filectime($resizedDir . DIRECTORY_SEPARATOR . $wantedFileName) > filectime($filepath)) {
					Image::fromFile($resizedDir . DIRECTORY_SEPARATOR . $wantedFileName)->send(Image::JPEG);
					$this->terminate();
				}
			}

//        image hasn't been resized yet, here we go
			$image = Image::fromFile($filepath);
			$method = $crop == "crop" ? Image::EXACT : Image::FIT;
			$image->resize($width, $height, $method);
			$image->save($resizedDir . DIRECTORY_SEPARATOR . $wantedFileName, 90);
			$image->send(Image::JPEG);
			$this->terminate();
		} catch (UnknownImageFileException $e) {
			$this->sendError(\StatusCodes::HTTP_INTERNAL_SERVER_ERROR, $e->getMessage());
		}
	}

}
