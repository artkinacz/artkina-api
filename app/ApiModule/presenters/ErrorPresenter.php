<?php
namespace App\ApiModule;

use Model;
use Nette;
use Tracy\Debugger;

class ErrorPresenter extends Nette\Object implements Nette\Application\IPresenter
{

	public function run(Nette\Application\Request $request)
	{
		$e = $request->parameters['exception'];
		if ($e instanceof Nette\Application\BadRequestException) {
			$code = $e->getCode();
		} else {
			$code = 500;
			Debugger::log($e, Debugger::ERROR);
		}
		ob_start();
		require __DIR__ . '/templates/error.phtml';

		return new Nette\Application\Responses\TextResponse(ob_get_clean());
	}

}