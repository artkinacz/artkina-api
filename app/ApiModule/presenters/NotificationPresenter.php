<?php
namespace App\ApiModule;

use App\Doctrine\Entity\Notification;
use App\Doctrine\Entity\Projection;
use App\Doctrine\Facade\NotificationFacade;
use App\Doctrine\Facade\ProjectionFacade;
use Nette\Utils\ArrayHash;
use Nette\Utils\DateTime;

class NotificationPresenter extends BasePresenter
{

	/**
	 * @var NotificationFacade @autowire
	 */
	public $notificationFacade;

	/**
	 * @var ProjectionFacade @autowire
	 */
	public $projectionFacade;

	/**
	 * Adds notification for given user-projection-timestamp combination.
	 * @param $type
	 * @param $userId
	 * @param $projectionId
	 * @param $minutes
	 */
	public function actionAdd($type, $userId, $projectionId, $minutes)
	{
		if (!isset(Notification::$types[$type])) {
			$this->sendError(\StatusCodes::HTTP_BAD_REQUEST,
				"Invalid type (allowed types: " . implode(" ,", Notification::$types[$type]));
		}
		if (!is_numeric($minutes)) {
			$this->sendError(\StatusCodes::HTTP_BAD_REQUEST, "Minutes must be a number");
		}

		$type = Notification::$types[$type];
		// Test if projection exists.
		/** @var Projection $projection */
		$projection = $this->projectionFacade->findOneBy(array("id" => $projectionId));
		if (!$projection) {
			$this->sendError(\StatusCodes::HTTP_NOT_FOUND, "Projection not found by given ID #$projectionId");
		}

		$targetDatetime = clone $projection->getDate();
		$targetDatetime->modify("- $minutes minutes");

		// Try to find this combination.
		/** @var Notification $notif */
		$notif = $this->notificationFacade->findOneBy(array(
			'userId' => $userId,
			'projection' => $projection
		));

		// Test correct notification timing.
		$result = $this->testTimes($targetDatetime, $projection->getDate());

		// If it does not exist, create it.
		if (!$notif) {
			$notif = new Notification();
			$notif->setUserId($userId)->setProjection($projection)->setTimestamp($targetDatetime)->setType($type);
		} else {
//            else, update it
			$notif->setTimestamp($targetDatetime);
		}
		$this->notificationFacade->save($notif);
		$notif = ArrayHash::from($notif->toArray());
		$notif->time_before = ($projection->getDate()->getTimestamp() - $targetDatetime->getTimestamp()) / 60;
		$this->setData($notif);

	}

	public function actionSend($id)
	{
		$notification = $this->notificationFacade->findOneBy(array("userId" => $id));
		dump($notification);
		die();
	}

	/**
	 * @param int $notifId
	 * @param int $minutes
	 */
	public function actionEdit($notifId, $minutes)
	{

		// Try to find notification record.
		/** @var Notification $notif */
		$notif = $this->notificationFacade->findOneBy(array("id" => $notifId));

		if (!$notif) {
			$this->sendError(\StatusCodes::HTTP_NOT_FOUND, "Notification not found by given ID #$notifId");
		}
		$targetDatetime = clone $notif->getTimestamp();
		$targetDatetime->modify("- $minutes minutes");

		// If it exists, find the corresponding projection.
		/** @var Projection $projection */
		$projection = $notif->projection;

		// Test correct notification timing.
		$result = $this->testTimes($targetDatetime, $projection->getDate());

		// Update notification time.
		$notif->setTimestamp($targetDatetime);
		$notif->lastModified = new DateTime();
		$this->notificationFacade->save($notif);
		$this->setData($notif->toArray());
	}

	public function actionDelete($notifId)
	{
		// Try to find notification record.
		$notif = $this->notificationFacade->findOneBy(array("id" => $notifId));

		if (!$notif) {
			$this->sendError(\StatusCodes::HTTP_NOT_FOUND, "Notification not found by given ID #$notifId");
		}

		$this->notificationFacade->delete($notif);
		$this->setStatus(\StatusCodes::HTTP_OK, "Notification deleted.");
	}

	public function actionGet($notifId)
	{
		// Try to find notification record.
		$notif = $this->notificationFacade->findOneBy(array("id" => $notifId));

		if (!$notif) {
			$this->sendError(\StatusCodes::HTTP_NOT_FOUND, "Notification not found by given ID #$notifId");
		}

		$projectionStart = $notif->projection->getDate();
		$notificationStart = $notif->getTimestamp();
		$diff = ($projectionStart->getTimestamp() - $notificationStart->getTimestamp()) / 60;

		$this->setData(array(
			'id' => $notif->id,
			'user_id' => $notif->userId,
			'projection_id' => $notif->projection->getId(),
			'timestamp' => $notif->timestamp,
			'time_before' => $diff,
			'last_modified' => $notif->lastModified,
		));
	}

	public function actionActive($userId)
	{

		// Try to find notification record.
		$notifications = $this->notificationFacade->createQuery("SELECT n FROM App\Doctrine\Entity\Notification n WHERE n.userId = :userId AND n.timestamp > CURRENT_TIMESTAMP()")
			->setParameters(array("userId" => $userId))
			->getResult();
		if (!$notifications) {
			$this->sendError(\StatusCodes::HTTP_NOT_FOUND, "Notification not found by given USER ID #$userId");
		}

		$data = array();
		foreach ($notifications as $notif) {
			/** @var Notification $notif */
			$projectionStart = $notif->projection->getDate();
			$notificationStart = $notif->getTimestamp();
			$diff = ($projectionStart->getTimestamp() - $notificationStart->getTimestamp()) / 60;
			$data[] = array(
				'id' => $notif->id,
				'user_id' => $notif->userId,
				'projection_id' => $notif->projection->getId(),
				'timestamp' => $notif->timestamp,
				'time_before' => $diff,
				'last_modified' => $notif->lastModified,
			);
		}

		$this->setData($data);
	}

	/**
	 * Tests correct timing of notification.
	 *
	 * @param string $notificationDate Date format string
	 * @param string $projectionDate Date format string
	 * @return boolean|string TRUE or error response (JSON encoded)
	 */
	protected function testTimes(\DateTime $notificationDate, \DateTime $projectionDate)
	{
		$currentDate = new DateTime();

		// Test if projection time is in the future (not in the past or right now).
		if ($projectionDate <= $currentDate) {
			$this->sendError(\StatusCodes::HTTP_BAD_REQUEST,
				'Projection already passed on ' . $projectionDate->format('d.m.Y H:i') . '.');
		}

		// Test if notification time is in the future (not in the past or right now).
		if ($notificationDate <= $currentDate) {
			$this->sendError(\StatusCodes::HTTP_BAD_REQUEST,
				'Notification must take place in the future (instead, ' . $notificationDate->format('d.m.Y H:i') . ' was given while current time is ' . date_format($currentDate,
					'd.m.Y H:i') . ').');
		}

		// Test if the notification is placed before the projection.
		if ($notificationDate >= $projectionDate) {
			$this->sendError(\StatusCodes::HTTP_BAD_REQUEST,
				'Notification time (' . $notificationDate->format('d.m.Y H:i') . ') must be set prior to the projection time (' . $projectionDate->format('d.m.Y H:i') . ').');
		}

		return TRUE;
	}

}