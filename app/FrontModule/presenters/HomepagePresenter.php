<?php

namespace App\FrontModule;

use App\BasePresenter;


/**
 * Homepage presenter.
 */
class HomepagePresenter extends BasePresenter
{

	public function renderDefault()
	{
	}

}