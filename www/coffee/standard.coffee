$('document')
	.ready ->

		tracker = new Boba
		tracker.trackLinks()
		
		play = this
		playTwo = this
		$('.waypoint')
			.waypoint((direction) ->
				wid = $(this).attr 'id'
				if direction == 'up'
					highlight_p = $('#' + wid).waypoint 'prev'
					wid_p = highlight_p.attr('id')
					$('nav')
						.find('a')
						.removeClass 'active'
					$('nav a[data-wp=' + wid_p + ']')
						.addClass 'active'
				else if direction == 'down'
					$('nav')
						.find('a')
						.removeClass 'active'
					$('nav a[data-wp=' + wid + ']')
						.addClass 'active'
			, offset: 100
			)
			
			$('nav').find('a').click (e) ->
				e.preventDefault()
				$(this).siblings().removeClass 'active'
				$(this).addClass 'active'

				correction = 0
				correction = -100 if $(this).attr('href') is '#story'
				correction = -80 if $(this).attr('href') is '#download'

				$('html, body').animate
			        scrollTop: $($(this).attr('href')).offset().top + correction
			    	2000
						
			slide = ->
					first = $('.slideshow img').first()
					last = $('.slideshow img').last()
					next = first.next()

					first.fadeTo 'fast', 0
					next.fadeTo 'fast', 1, () ->
						last.after first.remove();

					next = $('.pager li a.active')
								.removeClass('active')
								.parent()
								.next()
					
					if next.length 
						next
							.children()
							.addClass 'active'
					else
						$('.pager li a')
							.first()
							.addClass 'active'
	
			
			$('.pager li a')
				.click (e) ->
					e.preventDefault()
					clearInterval play
					that = this
					$('.pager li a')
						.removeClass 'active'
					$(this)
						.addClass 'active'
	
					z = $('.slideshow img').first().fadeTo 'fast', 0, ->
							while ($(that).attr('data-slider-number') != z.attr('data-slide'))
								$('.slideshow img').last().after(z.remove())
								z = $('.slideshow img').first()
							
							$('.slideshow img').first().fadeTo 'fast', 1
							doSlide();
						
			doSlide = ->
				play = setInterval ->
					slide()
				, 6000
	
			doSlide()


			slideWidth = 140
			carouselSize = $('.cinemas-list img').size() - 2

			$('.cinemas-list a').each (index) ->  
				newPosition = index * slideWidth
				$(this).css 'left', "#{newPosition}px"

			rotate = -> 
				o = $('.cinemas-list a').first()
				p = $('.cinemas-list a').last()
				$('.cinemas-list a').animate(
					left: '-='+slideWidth
				, 400, -> 
					p.css('left', carouselSize*140).after(o)
				)

			doRotate = -> 
				playTwo = setInterval( -> 
					rotate()
				, 3000)
		
			doRotate()
			
			
enquire.register("screen and (min-width: 1261px)", {

	match: -> $('.slideshow img').fadeTo('fast', 0).first().fadeTo 'fast', 1
	unmatch: -> $('.slideshow img').hide();
	setup: ->
	deferSetup: true
	   
});